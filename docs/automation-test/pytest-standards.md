# pytest规范

[返回上一级](index.md)

## 一、fixture对象

> 代码位置: [vats/viper_case/base]

<details>
    <summary>举例</summary>
    
    ```python
    import pytest
    from viper_case.base.dataio import DataIO, Service_CMS, Service_PIS, Service_IIS
    
    @pytest.fixture(scope="class")
    def fixture_dataio(flow, fixture_parameters):
        dataio = DataIO(flow)
        dataio.prepare(fixture_parameters)
        yield dataio
        dataio.do_cleanup()
    ```
</details>

## 二、Service Class
> 代码位置: [vats/viper_case/base] <br/>
> “函数”(function/method)如何绑定到service class？<br/>
>> package/moudle绑定为service class的类属性<br/>
>> 这个目的是解决fixtue_parameters中，函数字段路径编写不方便的问题<br/>

<details>
    <summary>举例</summary>

    ```python
    class Service_IIS(ServiceBase):
    iis_api = dataio_func.IIS  
    iis_business = business_method.iis_business  
    iis_boundary = boundary_method.iis
    
    def __init__(self, config: Dict):
        ServiceBase.__init__(self)
        self.config = config
        self.host = config.get("host")
        setattr(dataio_func.IIS, "host", self.host)
        self.logger = logger
    ```
</details>
 
## 三、测试用例
> 测试用例位置按原来位置放即可，保持为pytest风格就行
### 3.1 fixture_parameters中setup/teardown function表现形式

#### 3.1.1 如果函数是要通过类属性间接引用的

> function： 通过service class通过IDE“点”出来函数

#### 3.1.2 如果函数是service class对象的方法
> function： 填写方法名

### 3.2 测试步骤

> fixture对象通过IDE“点”出来函数作为测试步骤执行<br/>
> 总结：setup/teststep/teardown函数通过serviceClass类/对象引用即可，不需要import导入<br/>

<details>
    <summary>举例</summary>

    ```python
    import pytest
    from viper_case.base.dataio import DataIO
    
    class TestBringLunch:
        @pytest.fixture(scope="class")
        def fixture_parameters(self):
            return {
                # 3.1、如果函数是要通过类属性间接引用的
                "detail": { 
                    # "function": "business.for_demo.do_something",     # before: 需要写长的属性路径
                    "function": DataIO.business.for_demo.do_something,  # now： 通过service class通过IDE“点”出来函数
                },
                # 3.2、如果函数是service class对象的方法
                "food": {
                    "function": "cook_food" # service class对象方法名
                }
                
            }
    
        def test_eat_lunch1(self, fixture_dataio: DataIO):
            # 测试步骤
            fixture_dataio.business.for_demo.do_something
    ```
</details>

## 四、其他

<details>
    <summary>demo示例</summary>

    ```python
    ├── 1_test_step
    │   ├── test_1teststep_presenting-func.py # 引用函数库中function/method作测试步骤teststep
    │   ├── test_2modify_teststep_desc.py # 装饰器set_desc更改测试步骤描述信息
    │   └── test_3use_allure_operation_decorator.py # 临时封装函数,使用allure_operation装饰器记录测试步骤详情
    ├── 2_class_setup_teardown
    │   ├── setup
    │   │   ├── test_1setup_presenting-func.py # 引用函数库中function/method作setup操作
    │   │   ├── test_2modify_setup_desc.py # 设置desc字段覆盖setup默认描述信息
    │   │   ├── test_3way_for_noneed_return.py # 不需要用到setup返回值时的写法, 可省略function字段直接放在key中
    │   │   ├── test_4multi_funcs.py # setup需要多个function/method调用完成, function字段内部可嵌套 
    │   │   └── test_5multi_funcs2.py # setup需要多个function/method调用完成, 可封装成一个方法
    │   ├── teardown
    │   │   ├── test_1teardown_way1.py # 添加teardown方式一: add_cleanup (*args可变参数、**kwargs关键字参数)
    │   │   ├── test_2teardown_way2.py  # 添加teardown方式二: add_teardown (默认参数)
    │   │   └── test_3teardown_way3.py  # 添加teardown方式三: add_teardown_list 批量添加
    │   ├── test_1setup.py # setup实例
    │   ├── test_2teardown.py # teardown实例
    │   ├── test_3setup_teardown_host.py # setup/teardown中位置参数/默认参数有host可以不传, 默认补充
    │   └── test_4setup_teardown_config.py # setup/teardown参数使用到配置中的参数, 如何传参?
    ├── 3_method_setup_teardown
    │   ├── test_1basic.py # method级别的setup写法于class级别写法完全一致
    │   ├── test_2testset_setup.py  # 测试集setup - 只是测试集开始才执行一次
    │   ├── test_3testcase_setup.py  # 单个测试用例setup - 每个用例都执行一次
    │   ├── test_4setup_mix.py # setup中既有整个测试集级别的又有测试用例级别
    │   ├── test_5testset_teardown.py # 测试集eardown - 只是测试集结束才执行一次
    │   ├── test_6testcase_teardown.py # 单个测试用例teardown - 每个用例都执行一次
    │   ├── test_7teardown_mix.py. #  teardown中既有整个测试集级别的又有测试用例级别
    │   └── test_8setup_teardown_mix.py # 不同类型setup/teardown自由组合
    ├── 4_mix_setup_teardown
    │   ├── test_1.py # class级别setup/teardown与setup级别setup/teardown都存在
    │   └── test_2variables.py # method级别setup/teardown使用class级别setup返回值
    └── 5_supplyment
            ├── test_1class_stage.py # class级别setup/teardown操作可以直接在fixture中写【写法有要求】
            └── test_2method_stage.py # method级别setup/teardown操作可以直接在函数中写【写法有要求】
    ```
</details>





