# 单接口用例规范 & 编写方式

[返回上一级](index.md)

## 项目文件结构

<details>
    <summary>viper_case/api_test</summary>

    ```python
    ├── base         #  组件对象
    ├── services     # 单接口测试用例，按模块划分
       ├── dataio
       └── process
    └── test_files
       ├── json_files  # API接口默认请求body，按模块/组件划分
            ├── dataio
                  └── cms
            └── process
       ├── yml_files  # 测试数据，按模块/组件划分
            ├── dataio
            └── process
       └──auto_gen_cases.py  # 自动生成用例数据（用例生成后，根据接口情况进行调整）
    ├── conftest.py 
    ```
</details>

<details>
    <summary>viper_comon/api_test</summary>

    ```python
    ├── comparators.py                     #  放置内置/自定义断言方法
    ├── function_parse.py                  # 解析变量/函数（变量：$var/${var}, 函数：${func(a, b)}）
    ├── gen_var_functions                  # 放置${func(a, b)} 中的func
    ├── json_path.py                       # get && set json field by jsonpath
    ├── response.py                        # 接口响应处理（reponse数据、数据校验等）
    ├── step_allure.py                     # allure报告展示测试过程数据（前置依赖数据、API请求响应详情、断言结果列表详情）
    ├── tests_parse.py                     # 解析yaml单接口测试数据
    └── utils.py
    ```
</details>


## 单接口用例数据编写方式

> 测试数据由case_desc( 用例描述)、fields_set( 字段配置)、validators( 结果校验列表)三部分组成

1. jsonpath定位json字段

<details>
    <summary>jsonpath基本用法</summary>

    ## 语法参考：https://pypi.org/project/jsonpath-ng/

    ```json
    data = {
      "aa": "123",
      "camera_request": {
            "camera_parameter": {
                  "type": "FC_HK",
                  "face_camera": {
                        "host": "192.168.1.5",
                        "password": "panxianmin",
                        "port": 90,
                        "user": "panxianmin",
                        "dir": "dir",
                        "ipc": "ipc",
                  },
            },
            "host": "a.b.c.d",
            "display_name": "abcdefg",
            "description": "string",
            "tags": ["string", {"aa": 123, "bb": 456}],
            "geo_point": {"latitude": -90, "longitude": 100},
            "extra_info": "ajfkjka",
      },
    }
    ```
    
    ```py
    from jsonpath_ng import parse
    
    # 字段在顶层
    fields = parse("$.aa").find(data)
    print(len(fields), fields[0].full_path, fields[0].value)  # 1 aa 123

    # 字段不存在
    fields = parse("$.aa123").find(data)
    print(len(fields))   # 0

    # 嵌套字段
    fields = parse("$..password").find(data)
    print(len(fields), fields[0].full_path, fields[0].value) # 1 camera_request.camera_parameter.face_camera.password panxianmin

    # 找到多个时要锁定范围指定
    fields = parse("$..host").find(data)
    print(len(fields))  # 2
    fields = parse("$..face_camera.host").find(data)
    print(len(fields), fields[0].full_path, fields[0].value) # 1 camera_request.camera_parameter.face_camera.host 192.168.1.5

    # 字段是列表元素
    fields = parse("$..tags[0]").find(data)
    print(len(fields), fields[0].full_path, fields[0].value) #  1 camera_request.tags.[0] string

    fields = parse("$..tags[1]").find(data)
    print(len(fields), fields[0].full_path, fields[0].value) # 1 camera_request.tags.[1] {'aa': 123, 'bb': 456}
    ```
    ```txt
    注意：

    字段名称中包含"$"或"."符号时的写法
    {
        "aaa.bbb": {
          "$ccc": {
              "ddd.eee": 123
                   }
             }
    }
    # 以符号"^"开头 ，字段间的连接用符号"-"
    ^aaa.bbb               表示字段 "aaa.bbb",              值为{”aaa.bbb“: {"ddd.eee": 123}}
    ^aaa.bbb-$ccc          表示字段 "aaa.bbb.$ccc,          值为 {"ddd.eee": 123}
    ^aaa.bbb-$ccc-ddd.eee  表示字段 "aaa.bbb.$ccc.ddd.eee,  值为123
    ```
</details>

2. fields_set(字段配置)

<details>
    <summary>字段配置</summary>

    1. value为null/Null/NULL表示字段不配置，去除该字段
    2. none/None/NONE表示字段值配置为None
    3. path params配置字段不传时，配置空字符串即可
</details>

3. validators(结果校验列表)

注意：校验表达式，被校验项和预期值于断言方式之间需要至少有一个空格！

<details>
    <summary>结果校验</summary>

    ```txt
    常规写法:
       - [校验响应status_code, $..display_name=abcde, status_code = 200]
       - [检验响应headers字段, $..display_name=abcde, headers.Connection = keep-alive]
       - [检查响应body字段, $..display_name=abcde, $..display_name == abcde]
       - [检查响应body字段2, $..display_name=abcde,$..ipc == ipc]
       - [多个校验方式, $..display_name=abcde, status_code = 200;headers.Connection = keep-alive;$..display_name == abcde]
    ```
    ```txt
    常用数字校验:
       - [检查方式 -> 等于, $..display_name=1, code eq 3]
       - [检查方式 -> 不等于, $..display_name=2, code ne 2]
       - [检查方式 -> 大于, $..display_name=3, code gt 2]
       - [检查方式 -> 小于, $..display_name=null, code lt 4]
       - [检查方式 -> 大于等于, $..display_name=null, code ge 3]
       - [检查方式 -> 小于等于, $..display_name=null, code le 3]
       - [检查方式 -> 等于(=), $..display_name=null, code = 3]
       - [检查方式 -> 不等于(!=), $..display_name=null, code != 2]
       - [检查方式 -> 大于(>), $..display_name=null, code > 2]
       - [检查方式 -> 小于(<), $..display_name=null, code < 4]
       - [检查方式 -> 大于等于(>=), $..display_name=null, code >= 3]
       - [检查方式 -> 小于等于(<=), $..display_name=null, code <= 3]
       - [检查方式 -> 类型校验, $..display_name=null, code type_is int]
    ```
    ```txt
    常用字符串校验:
       - [检查方式 -> 字符串长度等于, $..display_name=123456:str, $..display_name len_eq 6]
       - [检查方式 -> 字符串长度大于, $..display_name=123456:str, $..display_name len_gt 5]
       - [检查方式 -> 字符串长度大于等于, $..display_name=123456:str, $..display_name len_ge 6]
       - [检查方式 -> 字符串长度小于, $..display_name=123456:str, $..display_name len_lt 7]
       - [检查方式 -> 字符串长度小于等于, $..display_name=123456:str, $..display_name len_le 6]
       - [检查方式 -> 字符串长度不等于, $..display_name=123456:str, $..display_name len_ne 5]
       - [检查方式 -> 正则表达式, $..display_name=123456:str, '$host regex_match \b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b']
       - [检查方式 -> 包含子子字符串, $..display_name=123456:str, $..display_name contains 123:str]
       - [检查方式 -> in, $..display_name=123456:str, $..display_name in 123456789:str]
       - [检查方式 -> not_in, $..display_name=123456:str, $..display_name not_in 6666666:str]
       - [检查方式 -> 子字符串开头, $..display_name=123456:str, $..display_name startswith 123:str]
       - [检查方式 -> 子字符串结尾, $..display_name=123456:str, $..display_name endswith 456:str]
       - [检查方式 -> 变量类型, $..display_name=123456:str, $..display_name type_is str]
    ```
</details>

## 进阶
1. variables

> 变量替换
> 
>> 字符串里只有str/int/float可以直接用，其它类型list/dict/set/..    要通过变量

<details>
    <summary>变量替换</summary>
    
    variables:
      eq_var: "= = ="
      a_e: "abcde"
      right_code: 200
      code_list: [200, 300, 400]
    cases:
      - [常规写法,   $..display_name=abcde, status_code = 200]
      - [字段值由变量表示,   $..display_name=$a_e, status_code = 200]
      - [字段值包含=,   $..display_name=$eq_var, status_code = 200]
      - [校验值同样可以用变量,   $..display_name=abcde, status_code = $right_code]
      - [status_code可用值,   $..display_name=abcde, status_code in $code_list]
</details>

2. functions

> 支持变量递归调用，支持位置参数和默认参数

<details>
    <summary>变量递归调用</summary>
    
    variables:
      uuid: ${gen_uuid()}
      random_num1: ${gen_random_num(10, 10000, var_type=str)}
      num: ${gen_random_num(100, 1000)}
      num2: 10000
      var_type: str
      random_num2: ${gen_random_num($num, $num2, var_type=$var_type)} 

      # class 级别变量为所有用例公用
      session_vars:
            value1: ${gen_random_num(333, 444, var_type=str)}

            num: ${gen_random_num(666, 999)}
            num2: 8888
            var_type: str
            value2: ${gen_random_num($num, $num2, var_type=$var_type)} 
    cases:
      - [常规写法, $..display_name=abcde, status_code = 200]
      - [值为毫秒时间戳, '$..display_name=${gen_time_ms()}', status_code = 200]
      - [值为纳秒时间戳, '$..display_name=${gen_time_ns()}', status_code = 200]
      - [值为utc时间, '$..display_name=${gen_utc_time()}', status_code = 200]
      - [值为指定长度随机字符串, '$..display_name=${gen_random_str(6)}', status_code = 200]
      - [值为uuid1, '$..display_name=${gen_uuid()}', ' $..display_name = ${gen_uuid()}']
      - [值为uuid2, $..display_name=$uuid, $..display_name = $uuid]
      - [值为随机数字, $..display_name=$num, $..display_name = $num]
      - [值为随机数字字符串0, $..display_name=$random_num1, $..display_name = $random_num1] # random_num1为用例级别变量，两个用例中$random_num1值不›一致
      - [值为随机数字字符串1, $..display_name=$random_num1, $..display_name = $random_num1]
      - [值为随机数字字符串2, $..display_name=$random_num2, $..display_name = $random_num2]
      - [值为随机数字字符串3, $..display_name=$value1, $..display_name = $value1]   # value1为calss级别变量，两个用例中$value1值一致
      - [值为随机数字字符串4, $..display_name=$value1, $..display_name = $value1]
      - [值为随机数字字符串5, $..display_name=$value2, $..display_name = $value2]   # value2为calss级别变量，两个用例中$value2值一致
      - [值为随机数字字符串6, $..display_name=$value2, $..display_name = $value2]
</details>
      

## 配合工具自动生成常见用例数据

viper_case/api_test/test_files/auto_gen_cases.py

## 生成allure测试报告

1. 准备好执行脚本

将生成报告的脚本run.sh放于vats根目录下

```shell
# 删除已有allure报告
rm -rf allure-*
# 指定测试项执行
pytest -v --alluredir ./allure-results $1
# 生成allure报告
allure generate --clean ./allure-results -o ./allure-report
# 注入JavaScript代码对json数据进行格式化
cat > www.txt <<- EOF
<script>
      function format(elements){
            for (i = 0; i < elements.length; i++) {
                  ele = elements[i]
                  try {
                        innerText =   ele.innerText.replace(/False/g, 'false')
                        innerText =   innerText.replace(/True/g, 'true')
                        innerText =   innerText.replace(/None/g, 'null')
                        var text = eval("(" + innerText + ")");
                        var result = JSON.stringify(text, null, 4);
                        ele.innerText = result
                        ele.style.whiteSpace = "pre"
                  }
                  catch(err) {}
            }
      }
      function jsonFormat() {
            var elements1 = document.getElementsByClassName('parameters-table__cell_value');
            var elements2 = document.getElementsByTagName("span")
            format(elements1)
            format(elements2)
      }
      //每3秒执行一次jsonFormat方法
      window.setInterval("jsonFormat()", 5000);
</script>
</html>
EOF
sed -i 's/<\/html>//g' $PWD/allure-report/index.html
cat www.txt >>   $PWD/allure-report/index.html
# 报告重命名
new_name=allure-report-$(date -d today +"%Y-%m-%d_%H_%M_%S.%N")
mv allure-report $new_name
# 拷贝到远程NGINX服务器下
scp -r ./$new_name root@172.20.25.49:/home/public/dataio_reports
# 打印报告访问链接
echo http://172.20.25.49:9999/$new_name ' <<< click it! '
```


2. 指定测试项执行，如：
```shell
sh run.sh  /home/***/vats/viper_case/api_test/services/dataio/cms
/test_zone_list.py
# 点击返回的链接进行报告访问
```

3. 测试报告说明
![测试信息记录](attachments/img001.png)
