# pytest自动化测试框架分享

[返回上一级](index.md)

## 一、功能介绍
### 1.1 统一规范
#### 1.1.1 统一使用fixture，避免fixture杂乱 [fixture是pytest优秀特性，但是杂乱无章反倒是累赘]
> fixture1: 在conftest.py中，scope为class，返回ServiceBase对象 <br/>
> fixture2: 在TestClass中，scope为class，配置class级别setup/teardown <br/>
> fixture1依赖于fixture2，所以先执行fixture2，再执行fixture1 <br/>

#### 1.1.2 统一函数调用方式，避免import杂乱
> setup/call/teardown中各个操作都通过同一个ServiceBase对象

#### 1.1.3 此模式单接口与业务流统一使用

### 1.2 支持test_class、 test_method级别setup/teardown
> test_class级别setup/teardown，对应测试任务 <br/>
> 通过装饰器cases_setup_teardown实现method级别setup/teardown，对应测试集/测试用例 <br/>
> test_class、 test_method级别setup/teardown分位置在allure报告中记录详细信息 <br/>

### 1.3 支持记录测试步骤详情，格式化展示（如：开发/测试debug时依赖的API请求响应详情都可以记录)
> 通过装饰器allure_operation实现 <br/>
> 有默认标题并可通过装饰器set_desc自定义更改 <br/>
> image base64超长数据默认省略显示，点击可展开全部数据 <br/>

### 1.4 解决现有痛点问题，支持记录 pod在用例执行期间的日志以及pod重启失败原因日志(点击图标按钮可新打开网页查看)
> 通过装饰器assign_pod_labels实现 <br/>
> 默认用例失败时才记录，可通过在配置中添加show_pod_logs字段，用例不管成功或失败都记录用例执行期间的pod日志

备注： 1项为基本功能， 2、3、4项为扩展功能，可通过引入对应装饰器按需灵活使用 【viper_case/base/utils/decorator.py】

## 二、自动化测试架构图

![svg001](attachments/svg001.svg)

<details>
    <summary>核心代码目录</summary>

    ```
    # 代码位置： viper_case/base
    ├── dataio.py        # 各组件服务类
    ├── featuredb.py
    ├── infra.py
    ├── __init__.py
    ├── service_base.py   # 服务基础类ServiceBase
    └── utils
        ├── decorator.py   # 功能扩展装饰器
        ├── __init__.py
        ├── models.py      # "函数"数据结构
        └── step_allure.py # allure报告记录
    ```
</details>


## 三、概念梳理

### 3.1 测试用例 - 执行过程
#### 3.1.1 Setup: 执行系列setup操作，进行环境初始化、生成前置依赖数据等
```text
setup_list: 
    - run_setup1    # 注意：setup其实有两种类型
    - run_setup2    # 一种生成是用例步骤执行所要的依赖数据，即setup操作执行需要返回值
    ...             # 另一种是类似于更改配置等环境初始化操作，setup操作执行不需要有返回值
    - run_setupN
```

#### 3.1.2 Call: 按照用例设计执行系列测试步骤　
```text
test_steps:
    - execute_test_step1、assert1
    - execute_test_step2、assert2
    ...
    - execute_test_stepN、assertN
```

#### 3.1.3 Teardown: 执行系列teardown操作，进行进行环境恢复、用例生成的脏数据清理等
```text
teardown_list:
    - run_teardown1
    - run_teardown2
    ...
    - run_teardownN　
```

### 3.2 "函数库"的概念 - setup/teststep/teardown以"函数"方式执行
#### 3.2.1 "函数"分类

> 根据3.1，我们可以发现整个测试用例执行过程分为setup操作、测试步骤和teardown操作 <br/>
> 系列setup/test_step/teardown都可以以函数作为最小单位来执行，方便执行和记录执行详情 <br/>
> 而"函数"的表现形式有多种： <br/>
>>  - function <br/>
>>  - 类方法 <br/>
>>  - 实例方法 <br/>

> viper_interface中的各个API，以及viper_common封装的各种类[classmethod、类实例对象方法]、函数，都是"函数"
这些"函数"的集合我们可以定义为"函数库"

> 可以将"函数库"与"SeviceBase"对象通过属性/方法的方式建立关系，那么就可以通过"SeviceBase"对象引用"函数库"中任意函数"了
也就是说，可以通过"SeviceBase"对象引用"函数库"中任意函数来自由挑选组合我们需要的setup/test_step/teardown操作

#### 3.2.2 在此框架下的函数表现形式：

[viper_case/base/utils/models.py]

> 描述函数的数据模型(为防止使用者填错数据格式，做强类型校验) <br/>       

<details>
    <summary>class Function1(BaseModel)</summary>

    ```text 
    class Function1(BaseModel):
        desc: Optional[Text]   # 描述setup/teardown的是用来干嘛的
        function: Optional[Union[Text, FunctionType, MethodType]]  # 执行函数
        args: Optional[Tuple] = tuple()  # 位置参数, 用关键字参数args表示位置参数的集合
        kwargs: Optional[Dict] = dict()  # 默认参数, 用命名关键字参数kwargs表示默认参数的集合
        is_shared: bool = True # 默认为True. 表示在test_method决定是否为多个用例所共享 
    ```
</details>


> function可以是单个函数，也可以是多个函数的集合 ==> 可嵌套使用Function1

<details>
    <summary>class Function2(BaseModel)</summary>

    ```text 
    class Function2(BaseModel):
        desc: Optional[Text] 
        function: Optional[Union[Text, FunctionType, MethodType, List[Tuple[Text, Function1]]]]
        args: Optional[Tuple] = tuple()
        kwargs: Optional[Dict] = dict()
        is_shared: bool = True
    ```
</details>

#### 3.2.3 举例：
> 引入package/module/class/instance，绑定到类实例的属性上

<details>
    <summary>举例</summary>
    
    ```text
    from viper_common.ingress import boundary_method, business_method 
    from viper_interface.func import dataio_func
    class Service_CMS(ServiceBase):
        def __init__(self, config):
            ServiceBase.__init__(self)
            self.config = config
            self.host = config.get("host")
            setattr(dataio_func.CMS, "host", self.host)
            self.cms_api = dataio_func.CMS  # API的封装都在api属性里
            self.cms_business = business_method.cms_business  # 之前业务的封装都在business属性里
            self.cms_boundary = boundary_method.cms  # 之前用于setup/teardwon等方法放在boundary属性中
            self.logger = logger
    如：
    cms = Service_CMS(config)
        # function
            更改cms关于VPS的mock地址：cms.cms_boundary.set_vps_endpoint
        # classmethod
            调用CMS的API - ZoneNew: cms.cms_api.new_zone  
        # 实例方法
            创建区域：cms.create_zone_with_cleanup
            
    # 总结：这些函数可以在setup/test_setp/teardown中被调用执行
    
    补充说明: 
        vats中viper_common内封装的函数基本都要传入host参数, 作为setup/teardown执行时目前已做特殊处理, 有host参数可以不传,调用时内部自动填入。   
    ```
</details>

#### 3.2.4 图示：

![svg002](attachments/svg002.svg)

### 3.3 测试任务、测试集与测试用例
```text
class TestClass():  
    def test_method1():
        # execute_test_step1、assert1
        # execute_test_step2、assert2
        ......
        # execute_test_stepN、assertN
    
    @pytest.mark.parametrize("case", [test1, test2 ... testN])    
    def test_method2():
        # execute_test_step1、assert1
        # execute_test_step2、assert2
        ......
        # execute_test_stepN、assertN

test_method并不是测试用例的最小单位，TestClass中不是只有2个测试用例, 而是有N+1个测试用例
可以把test_method理解为一个测试集: 
    - test_method2是包含N个用例的测试集
    - test_method1也是一个测试集, 只不过这个测试集中只有一个测试用例而已
可以把TestClass理解成测试任务：
    - 包含test_method1、test_method2两个测试集
    - 测试总数量有N+1个        
```

### 3.4 Setup/Teardown 
#### 3.4.1 setup/teardown - 分类和实现方式

> setup/teardown分类 <br/>
>> 结合3.1和3.2，用例执行前后分别由setup和teardown做相应的前置和后置处理 <br/>
>> 在测试任务、测试集与测试用例的分层机制下，根据共享原则setup和teardown可对应分成以下几种类型： <br/>
>> 1、测试任务级别的setup、teardow  # 测试任务下的所有用例都可以共享使用 <br/>
>> 2、测试集级别的setup、teardow    # 测试集下的所有用例都可以共享使用 <br/>
>> 3、测试用例级别的setup、teardow  # 特定测试用例下使用

#### 3.4.2 场景举例：
```text
data000 = get_data000()
class TestClass():      # 测试任务级别: 所有用例执行前生成一个依赖数据data000为测试任务内用例使用，在所有用例执行完后进行清理
    data111 = get_data111()
    def test_method1():  # 除依赖数据data000外，本用例/测试集要生成一个依赖数据data111，用例/测试集执行完之后进行清理
        use_data000()
        use_data111()    
            
    data222 = get_data222() # M个用例执行M次
    @pytest.mark.parametrize("case", [test21, test22 ... test2M])    
    def test_method2():  # 除依赖数据data000外，每个用例都需要生成一个依赖数据data222，用例执行完之后进行清理
        use_data000()
        use_data222()   
     
    data333 = get_data333() # N个用例只需要执行一次
    @pytest.mark.parametrize("case", [test31, test32 ... test3N])    
    def test_method3():  #  除依赖数据data000外，整个测试集公用一个依赖数据data333，测试集执行完后进行清理
        use_data000()
        use_data333() 
```

#### 3.4.3 问题来了，如何实现？？？

> 测试任务级的setup/teardown通过scope为class的fixture实现 <br/>
>> - 原理：fixture中，yield关键字前执行setup操作，yield关键字后执行teardown操作 <br/>
>
> 测试集和测试用例的setup/teardown通过装饰器cases_setup_teardown实现 [装饰器位于viper_case/base/utils/decorator.py] <br/>
>> - 原理：test_method被装饰器装饰，内部test_method执行前后分别执行setup和teardown操作 <br/>
>> - 其中， <br/>
>> - setup操作中，is_shared字段为True的操作,表示为整个测试集共享 <br/>
>> - setup操作中，is_shared字段为False的操作,表示每个测试用例独立使用 <br/>
>
> 备注： 为什么不使用scope为function的fixture来实现测试集和测试用例级别的setup/teardown? <br/>
>> - scope为function的fixtur仍然是每个用例独立执行，无法做到测试集级别的setup/teardown <br/>
>> - 测试集和测试用例级别的setup/teardown与测试任务级别的保持一致即可，无需引入新的写法增加学习成本！

#### 3.4.4 Parameters[Setup数据] - 编写规范
> 简单说，就是一些要执行的函数，这些函数用作setup操作，setup的行为主要是以下方面： <br/>
>> 功能1： 生成用例必须依赖的前置数据（函数需要返回数据） <br/>
>> 功能2：提供用例必要的环境（函数不需要返回数据） <br/>
>> 功能3:   注入teardown操作，待用例结束后执行 

```text
    类型：dict         
         key、value规范1:  # 适用于, setup的返回值是用例需要的前置依赖数据
            1、key: setup_results中的字段名称, setup操作的返回值放于对象setup_results属性中,可通过key对应获取
            2、value：字典 ->  viper_case/base/utils/models.py.Function2

         key、value规范2:  # 适用于不需要用到setup返回值,value中省去fucntion字段，直接放在key中
            1、key: 函数或方法对象/属性方法名称/属性方法路径[规范1中value的function字段]
            2、value：字典 ->  viper_case/base/utils/models.py.Function2
   
    注意: 
    1、 function(*args, **kwargs)后返回值，补充到value的"return"字段中  # 兼容之前的写法,可通过fixtrue_parameters获取返回值
    2、不推荐原来setup操作的返回值要通过fixtrue_parameters的方式获取, 通过setup_results属性获取即可。
    
    # 示例：      
        {
            一、需要用到setup的返回值
            # 1.1、function是self的属性方法名称,直接获取执行函数
            # 函数执行结果放于setup_results属性的"zone_aa"字段，其它同理
            "zone_aa": {"function": "create_zone_with_cleanup", "kwargs": {"prifix": "aa"}}, 
            # 1.2、function是以符号“.”为分隔符的属性路径, 可以通过self间接获得执行函数
            "notify": {"function": "cms_business.get_latest_result", "kwargs": {"dir_file": "platforms_status_notify"}},
            # 1.3、function属性方法对象
            "notify2": {"function": obj.cms_business.get_latest_result, "kwargs": {"dir_file": "platforms_status_notify"}},
            
            二、不需要获取setup返回值
            # 2.1、类比1.1
            "create_zone_with_cleanup": {"kwargs": {"prifix": "aa"}},
            # 2.2、类比1.2
            "cms_business.get_latest_result": {"kwargs": {"dir_file": "platforms_status_notify"}} 
            # 2.3、类比1.2
            obj.cms_business.get_latest_result: {"kwargs": {"dir_file": "platforms_status_notify"}}
            
            三、function值为函数操作集合
            "xyz": {
                "function":[
                    ("xxx", {"fucntion": "func_step1", "args": (...), "kwargs": {...}}),
                    ("yyy", {"fucntion": "func_step2", "args": (...), "kwargs": {...}}),
                    ("func_step3", {"args": (...), "kwargs": {...}})
                ],
                "is_shared": False
            }
           # 函数集合执行结果放于setup_results属性的"xyz"字段，其它同理

           四、前面都是要执行的setup操作，那如何指定要执行的teardown操作呢？
           # setup操作是： 调用add_cleanup/add_teardown/add_teardown_list对象方法添加
           # 添加后，会根据级别【测试任务/测试集/测试用例】在对应teardown位置执行
           # 可在[vats/viper_case/base/service_base.py]中查看函数传参情况
           "add_cleanup": {"desc": "恢复vps配置", "args": ("boundary.set_vps_endpoint",), "kwargs": {"set_type": 'recover'}},
           "add_teardown": {"kwargs": {
                "desc": "删除此gb28181平台相关"
                "function": "boundary.del_platform_relative_resources",
                "args": (123,)
             }
          }, 
          "add_teardown_list": {"args": ([{"function": "boundary.set_vps_endpoint", "kwargs": {"set_type": 'recover'}}], )}                                
        }
```

<details>
    <summary>示例</summary>

    ```python
    # 示例：      
        {
            一、需要用到setup的返回值
            # 1.1、function是self的属性方法名称,直接获取执行函数
            # 函数执行结果放于setup_results属性的"zone_aa"字段，其它同理
            "zone_aa": {"function": "create_zone_with_cleanup", "kwargs": {"prifix": "aa"}}, 
            # 1.2、function是以符号“.”为分隔符的属性路径, 可以通过self间接获得执行函数
            "notify": {"function": "cms_business.get_latest_result", "kwargs": {"dir_file": "platforms_status_notify"}},
            # 1.3、function属性方法对象
            "notify2": {"function": obj.cms_business.get_latest_result, "kwargs": {"dir_file": "platforms_status_notify"}},
            
            二、不需要获取setup返回值
            # 2.1、类比1.1
            "create_zone_with_cleanup": {"kwargs": {"prifix": "aa"}},
            # 2.2、类比1.2
            "cms_business.get_latest_result": {"kwargs": {"dir_file": "platforms_status_notify"}} 
            # 2.3、类比1.2
            obj.cms_business.get_latest_result: {"kwargs": {"dir_file": "platforms_status_notify"}}
            
            三、function值为函数操作集合
            "xyz": {
                "function":[
                    ("xxx", {"fucntion": "func_step1", "args": (...), "kwargs": {...}}),
                    ("yyy", {"fucntion": "func_step2", "args": (...), "kwargs": {...}}),
                    ("func_step3", {"args": (...), "kwargs": {...}})
                ],
                "is_shared": False
            }
           # 函数集合执行结果放于setup_results属性的"xyz"字段，其它同理

           四、前面都是要执行的setup操作，那如何指定要执行的teardown操作呢？
           # setup操作是： 调用add_cleanup/add_teardown/add_teardown_list对象方法添加
           # 添加后，会根据级别【测试任务/测试集/测试用例】在对应teardown位置执行
           # 可在[vats/viper_case/base/service_base.py]中查看函数传参情况
           "add_cleanup": {"desc": "恢复vps配置", "args": ("boundary.set_vps_endpoint",), "kwargs": {"set_type": 'recover'}},
           "add_teardown": {"kwargs": {
                "desc": "删除此gb28181平台相关"
                "function": "boundary.del_platform_relative_resources",
                "args": (123,)
             }
          }, 
          "add_teardown_list": {"args": ([{"function": "boundary.set_vps_endpoint", "kwargs": {"set_type": 'recover'}}], )}                                
        }
    ```
</details>

#### 3.4.5 setup/teardown - allure报告记录
> 目前已支持allure记录setup/teardown详细数据和执行结果,可在allure报告中查看和debug <br/>
> allure报告中必须详细记录setup/teardown的理由: <br/>
>> 1. 确认是否是真实执行了你所期望的M个setup、N个teardwon <br/>
>> 2. 每个setup/teardown的参数输入、执行结果的输出是否符合你的预期 <br/>
> 所以必须记录下来,方便debug, 不然你以为执行的可能并没有真的实行，需要有理有据的信息可以追溯 <br/>
> 注： <br/>
>> test_class级别setup:          记录在"Set up"中fixture的"SetupDetail - TestClass" <br/>
>> test_class级别teardown:       记录在"Set up"中fixture的"TeardownDetail - TestCalss" <br/>
>> test_method级别的setup:       记录在"Test body"的"SetupDetail - TestMethod" <br/>
>> test_method级别的teardown:    记录在"Test body"的"TeardownDetail - TestMethod"

### 3.5 测试步骤
> 通过装饰器allure_operation装饰的"函数"[函数/类方法/实例方法]，在测试用例中被调用既可以在allure中记录函数执行详情
>> [装饰器位于viper_case/base/utils/decorator.py]
```text
class CMS:
    @classmethod
    @allure_operation("CMS API[ZoneCameraNew] - 创建区域相机") # 其中allure_operation传入的参数为测试步骤描述
    def new_zone_camera(...):
        pass
        
# 在测试用例中，如果不想使用默认的测试步骤描述，可以使用装饰器set_desc来重新指定描述信息
def test_ZoneCameraTaskNew(self, index, media_protocol_type, fixture_dataio:DataIO, request):
    func = fixture_dataio.cms_api.new_zone_camera
    set_desc(func , "新建zone camera")(zone_id=uuid, camera_id=uuid, camera_parameter=camera_parameter))        
```
![img002](attachments/img002.png)

### 3.6 用例执行期间的pod日志记录
> 通过装饰器assign_pod_labels实现
>> [装饰器位于viper_case/base/utils/decorator.py]
```text
# 使用方式：
1、装饰TestClass，可以递归装饰所有test_method
@assign_pod_labels(["engine-cms"])
class TestMediaProtocolTye:
    pass

2、装饰test_method 
@assign_pod_labels(["engine-cms", "engine-vis"])
def test_ZoneCameraGenerateRTSPAddress(self, index, media_protocol_type, fixture_dataio:DataIO, request):
    pass
    
# 注意：若TestClass和test_method 均被装饰，test_method优先级高

实现原理：
    1、装饰器将pod标签绑定test_method的属性中
    2、pytest hook - pytest_runtest_setup记录用例start_time
    3、pytest hook - pytest_runtest_makereport根据setup/call/teardown阶段是否有fail记录pod日志
# 注意： 默认只有在setup/call/teardown阶段有fail才记录pod日志，如果无论pass/fail都记录的话，可以在config中增减show_pod_logs字段    
```
![img003](attachments/img003.png)

## 四、实例演示
> 看了概念还是很模糊？ <br/>
> 没关系，现在是用demo示例手把手带你入门 <br/>
>> 1、执行原理断点调试 <br/>
>> 2、函数库如何绑定到executor中? <br/>  
>> 3、如何引用函数库中的函数作test_step/setup/teardown？ <br/>
>> 4、装饰器介绍 <br/>
>>> - 装饰器allure_operation、set_desc介绍 <br/>
>>> - 装饰器cases_setup_teardown介绍 <br/>
>>> - 装饰器assign_pod_labels介绍 <br/>

<details>
    <summary>demo_show</summary>

    ```text
    ├── 1_test_step
    │   ├── test_1teststep_presenting-func.py # 引用函数库中function/method作测试步骤teststep
    │   ├── test_2modify_teststep_desc.py # 装饰器set_desc更改测试步骤描述信息
    │   └── test_3use_allure_operation_decorator.py # 临时封装函数,使用allure_operation装饰器记录测试步骤详情
    ├── 2_class_setup_teardown
    │   ├── setup
    │   │   ├── test_1setup_presenting-func.py # 引用函数库中function/method作setup操作
    │   │   ├── test_2modify_setup_desc.py # 设置desc字段覆盖setup默认描述信息
    │   │   ├── test_3way_for_noneed_return.py # 不需要用到setup返回值时的写法, 可省略function字段直接放在key中
    │   │   ├── test_4multi_funcs.py # setup需要多个function/method调用完成, function字段内部可嵌套 
    │   │   └── test_5multi_funcs2.py # setup需要多个function/method调用完成, 可封装成一个方法
    │   ├── teardown
    │   │   ├── test_1teardown_way1.py # 添加teardown方式一: add_cleanup (*args可变参数、**kwargs关键字参数)
    │   │   ├── test_2teardown_way2.py  # 添加teardown方式二: add_teardown (默认参数)
    │   │   └── test_3teardown_way3.py  # 添加teardown方式三: add_teardown_list 批量添加
    │   ├── test_1setup.py # setup实例
    │   ├── test_2teardown.py # teardown实例
    │   ├── test_3setup_teardown_host.py # setup/teardown中位置参数/默认参数有host可以不传, 默认补充
    │   └── test_4setup_teardown_config.py # setup/teardown参数使用到配置中的参数, 如何传参?
    ├── 3_method_setup_teardown
    │   ├── test_1basic.py # method级别的setup写法于class级别写法完全一致
    │   ├── test_2testset_setup.py  # 测试集setup - 只是测试集开始才执行一次
    │   ├── test_3testcase_setup.py  # 单个测试用例setup - 每个用例都执行一次
    │   ├── test_4setup_mix.py # setup中既有整个测试集级别的又有测试用例级别
    │   ├── test_5testset_teardown.py # 测试集eardown - 只是测试集结束才执行一次
    │   ├── test_6testcase_teardown.py # 单个测试用例teardown - 每个用例都执行一次
    │   ├── test_7teardown_mix.py. #  teardown中既有整个测试集级别的又有测试用例级别
    │   └── test_8setup_teardown_mix.py # 不同类型setup/teardown自由组合
    ├── 4_mix_setup_teardown
    │   ├── test_1.py # class级别setup/teardown与setup级别setup/teardown都存在
    │   └── test_2variables.py # method级别setup/teardown使用class级别setup返回值
    └── 5_supplyment
            ├── test_1class_stage.py # class级别setup/teardown操作可以直接在fixture中写【写法有要求】
            └── test_2method_stage.py # method级别setup/teardown操作可以直接在函数中写【写法有要求】
    ```
</details>








