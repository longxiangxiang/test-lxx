#!/usr/bin/bash

sd=$1

if [[ x$sd == x ]] ;then
  echo "请输入需要加密的磁盘，例："
  echo "./encryption_mount.sh sdb"
  exit
fi

command -v cryptsetup
if [ $? -eq 1 ];then
  echo "not found cryptsetup ,now install .."
  kernel_name=`uname -r`
  judge_str="aarch"
  if [[ ${kernel_name} =~ $judge_str ]] ;then
    echo "This machine is arm server."
    yum -y install arm/cryptsetup-libs-2.0.3-6.el7.aarch64.rpm
    yum -y install arm/cryptsetup-2.0.3-6.el7.aarch64.rpm
  else
    echo "This machine is x86 server."
    yum -y install x86/json-c-0.11-4.el7_0.x86_64.rpm
    yum -y install x86/cryptsetup-libs-2.0.3-6.el7.x86_64.rpm
    yum -y install x86/cryptsetup-2.0.3-6.el7.x86_64.rpm
  fi
else
  echo "found cryptsetup , now continue ."
fi

cryptsetup luksFormat /dev/$sd <<EOF
88stIVA#2017
EOF

dd if=/dev/$sd of=/root/enc.key bs=1 count=4096

cryptsetup luksAddKey /dev/$sd /root/enc.key <<EOF
88stIVA#2017
EOF

cryptsetup luksOpen -d /root/enc.key /dev/$sd sc_data

mkfs.xfs /dev/mapper/sc_data

mkdir -p /admin

cat /etc/fstab|grep '/dev/mapper/sc_data' >/dev/null
if [[ $? == 1 ]] ;then
  echo '/dev/mapper/sc_data /admin xfs defaults 0 0' >> /etc/fstab
else
  echo "请检查'/dev/mapper/sc_data /admin xfs defaults 0 0'是否已经写到/etc/fstab中"
fi

cat /etc/crypttab|grep sc_data >/dev/null
if [[ $? == 1 ]] ;then
  echo 'sc_data /dev/'$sd' /root/enc.key' >> /etc/crypttab
else
  echo "请检查'sc_data /dev/$sd /root/enc.key是否已经写到/etc/crypttab中"
fi

mount -a

echo "注意将/root/enc.key文件多处备份，否则丢失之后将打不开硬盘"


