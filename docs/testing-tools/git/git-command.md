# git 常用命令

[返回上一级](index.md)

## 初始化仓库
```shell
git init
```

## 添加文件到暂存区
```shell
git add <file1> <file2> ...

# 添加所有文件
git add -A
```

## 提交暂存区的文件到本地仓库
```shell
git commit -m "commit message"
```

## 查看仓库状态
```shell
git status
```

## 查看提交记录
```shell
git log
```

## 查看提交记录简略信息
```shell
git log --oneline
```

## 创建新分支
```shell
git branch <branch-name>
```

## 切换分支
```shell
git checkout <branch-name>
```

## 合并分支
```shell
git merge <branch-name>
```

### 拉取远程仓库最新代码
```shell
git pull
```

## 推送本地提交到远程仓库
```shell
git push origin <branch-name>
```

## 克隆远程仓库
```shell
git clone <repository-url>
```

## 查看远程仓库列表
```shell
git remote -v
```

## 添加远程仓库
```shell
git remote add <remote-name> <repository-url>
```

## 从远程仓库获取最新代码但不合并
```shell
git fetch
```

## 撤销工作区的修改
```shell
git checkout -- <file>
```

## 撤销暂存区的修改
```shell
git reset HEAD <file>
```

## 重置最新的提交
```shell
git reset HEAD~1
```

## 回滚到特定提交
```shell
git reset --hard <commit-hash>
```

## 查看修改过的文件
```shell
git diff
```

## 查看提交之间的差异
```shell
git diff <commit1> <commit2>
```

## 查看特定文件的修改记录
```shell
git blame <file>
```

## 创建并切换到新分支
```shell
git checkout -b <new-branch-name>
```

## 合并特定提交到当前分支
```shell
git cherry-pick <commit-hash>
```

## 交互式地重新提交
```shell
git rebase -i <commit-hash>
```

## 暂存部分文件的修改
```shell
git add -p
```

## 删除远程分支
```shell
git push origin --delete <remote-branch-name>
```

## 清理本地无效分支
```shell
git remote prune origin
```

## 查看分支图
```shell
git log --graph --oneline --decorate --all
```

## 修改最新提交的提交信息
```shell
git commit --amend
```

## 使用 stash 暂存工作目录中的修改
```shell
git stash
```

## 从 stash 中应用修改
```shell
git stash apply
```
