# helm 常用命令

[返回上一级](index.md)

> Helm的三大概念 <br/>
>> Chart:  chart代表一个helm的包，这个包里会包含一切在k8s 上运行某个应用、工具、服务所需要的资源定义。你可以认为这个chart就是homebrew的formula，或者apt的dpkg，再或者是yum的rpm文件。<br/>
>> Repository：资料库嘛，估计愿意看这篇文章的同学也熟，就是在远端存chart的地方。<br/>
>> Release：release代表一个正运行在k8s 上的chart实例，一个chart可以在集群上安装多次，每安装一次，便会产生一个release。比如一个MySQL chart，你要是想在你集群上跑俩MySQL ，你就安两遍，产生两个release。<br/>

## 一、查看版本
```shell
helm version
```

## 二、查看当前安装的charts
```shell
helm list
```

## 三、查询 charts
```shell
helm search repo redis     # 在本地的repo list里面搜redis的chart

helm search hub redis      # 在helm hub上搜redis的chart
```

## 四、安装charts
```shell
helm install --name redis --namespaces prod bitnami/redis
#redis是releasename，bitnami/redis 是chart name

# 本地的Chart archive (helm install foo foo-0.1.1.tgz)
# 一个未打包的Chart 路径 (helm install foo path/to/foo)
# 一个完整的 URL (helm install foo  https://example.com/charts/foo-1.2.3.tgz)
```

## 五、查看charts状态
```shell
helm status redis
```

## 六、删除charts
```shell
helm delete --purge redis
```

## 七、增加repo
```shell
helm repo add stable  https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts

helm repo add --username admin --password password myharbor  https://harbor.qing.cn/chartrepo/charts

helm repo list
```

## 八、更新repo仓库资源
```shell
helm repo update
```

## 九、创建charts
```shell
helm create helm_charts
```

## 十、测试charts语法
```shell
helm lint
```

## 十一、打包charts
```shell
cd helm_charts && helm package ./
```

## 十二、查看生成的yaml文件
```shell
helm template helm_charts-0.1.1.tgz
```

## 十三、更新image
```shell
helm upgrade --set image.tag='v2019-05-09-18-48-40' study-api-en-oral myharbor/study-api-en-oral

helm upgrade --set foo=bar --set foo=newbar redis ./redis

helm upgrade [RELEASE] [CHART] [flags]

helm search repo | grep keystone-business-resource | awk '{print $1}'     #查看chartname

helm list | grep keystone-business-resource | awk '{print $1}'                          #查看release name

kubectl get cm helm-values -ojsonpath='{.data.global\.json}' > /tmp/global.json

helm upgrade --set min_start_days_after_now=0 keystone-business-resource-manager-service viper/keystone-business-resource-manager-service -f /tmp/global.json

helm get values keystone-business-resource-manager-service | grep min_start_days_after_now
```

## 十四、回滚relase
```shell
helm hist study-api-en-oral

helm rollback study-api-en-oral 4
```

## 十五、发布到私有harbor仓库脚本
```shell
request_url='https://harbor.qing.cn/api/chartrepo/charts/charts'
user_name='admin'
password='password'
chart_file='helm_charts-0.1.3.tgz'
curl -i -u "$user_name:$password" -k -X POST "${request_url}" \
-H "accept: application/json" \
-H "Content-Type: multipart/form-data" \
-F "chart=@${chart_file};type=application/x-compressed"

echo $result
helm show values stable/mariadb     #查看这个chart都支持哪些可配置项
```

```text
有两种方式在安装时传配置数据:
 --values  (or  -f): 指定一个yaml文件，这个参数可以传递多个，写指令时，越靠右的拥有越高的优先级。
 --set: 直接覆盖配置。
 如果都用的话，set的值会被合并到value里，并且拥有更高优先级。
 使用set复写的配置会被持久化到config map中，同时可以使用helm get values <release-name>获取那些通过set设置进来的值，想清除通过set进来的值可以在运行helm upgrade 时带上  --reset-values

--set 的格式与限制

--set  选项可以不指定参数，也可以指定多个name/value 对， 最简单的写法一般是:  --set name=value. YAML的等价写法:
name: value

多个值用 , 分割。 所以  --set a=b,c=d  yaml这里就变成了:
a: b
c: d

也有一些更复杂的形式，比如  --set outer.inner=value  会被识别成:
outer:
  inner: value

列表可用  {  和  }包裹。 比如，  --set name={a, b, c}  对应的是：
name:
  - a
  - b
  - c

到了 Helm 2.5.0，可以使用数组索引的方式去访问列表元素。比如  --set servers[0].port=80  就代表:
servers:
  - port: 80

多值可以这样设置。  --set servers[0].port=80,servers[0].host=example  代表:
servers:
  - port: 80
    host: example

有时你可能需要在  --set  里使用一些特殊字符。你可以用  \去做转义，比如  --set name=value1\,value2  的意思是:
name: "value1,value2"

类似的，你也可以给点儿做转义，这样写的  --set nodeSelector."kubernetes\.io/role"=master  会被理解成:
nodeSelector:
  kubernetes.io/role: master

深层嵌套的结构如果用  --set写起来可能会很复杂，所以Chart的编写者在设计变量时也要考虑其他使用者在使用  --set  进行赋值时书写的复杂度。
```
