# FFmpeg安装（本地编译）



> 本指南基于最新CentOS版本的最小安装，并将提供 FFmpeg 的本地非系统安装，并支持几个常见的外部编码库。这些说明也适用于最近的 Red Hat Enterprise Linux(RHEL)和Fedora

> 本指南设计为非侵入性的，将在您的主目录中创建多个目录：
>> ffmpeg_sources– 将下载源文件的位置。如果需要，可以在完成指南后将其删除。
>> ffmpeg_build– 将在哪里构建文件和安装库。如果需要，可以在完成指南后将其删除。
>> bin– 生成的二进制文件 (ffmpeg,ffprobe,x264,x265) 的安装位置。

You can easily undo any of this as shown in [Reverting Changes Made by This Guide](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#RevertingChangesmadebythisGuide).

Tip: Recent already compiled [static builds](https://ffmpeg.org/download.html) are also available if you are unable to compile or 
are impatient.

--------------------------------------------
## 安装依赖项

注意：该命令应以超级用户或 root 用户身份执行，并且仅在本指南中需要该yum命令
```shell
yum -y install autoconf automake bzip2 bzip2-devel cmake freetype-devel gcc gcc-c++ git libtool make pkgconfig zlib-devel
```

创建一个新目录以将所有源代码放入:
```shell
mkdir ~/ffmpeg_sources
```

---------------------------------------------
## 编译及安装

Tip: 如果您不需要某些编码器，您可以跳过相关部分，然后删除./configureFFmpeg 中的相应选项。例如，如果不需要libvpx，则跳过该部分，然后--enable-libvpx 
从 [安装 FFmpeg](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#FFmpeg) 部分中删除 

### NASM

```shell
cd ~/ffmpeg_sources
curl -O -L https://www.nasm.us/pub/nasm/releasebuilds/2.15.05/nasm-2.15.05.tar.bz2
tar xjvf nasm-2.15.05.tar.bz2
cd nasm-2.15.05
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
make
make install
```


### Yasm

```shell
cd ~/ffmpeg_sources
curl -O -L https://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
tar xzvf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
make
make install
```

## libx264

需要ffmpeg配置 --enable-gpl --enable-libx264
```shell
cd ~/ffmpeg_sources
git clone --branch stable --depth 1 https://code.videolan.org/videolan/x264.git
cd x264
PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static
make
make install
```

## libx265

需要ffmpeg配置--enable-gpl --enable-libx265
```shell
cd ~/ffmpeg_sources
git clone --branch stable --depth 2 https://bitbucket.org/multicoreware/x265_git
cd ~/ffmpeg_sources/x265_git/build/linux
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED:bool=off ../../source
make
make install
```

## libfdk_aac

需要ffmpeg配置--enable-libfdk_aac
```shell
cd ~/ffmpeg_sources
git clone --depth 1 https://github.com/mstorsjo/fdk-aac
cd fdk-aac
autoreconf -fiv
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
```


## libmp3lame

需要ffmpeg配置--enable-libmp3lame
```shell
cd ~/ffmpeg_sources
curl -O -L https://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
tar xzvf lame-3.100.tar.gz
cd lame-3.100
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared --enable-nasm
make
make install
```


## libopus

需要ffmpeg配置--enable-libopus
```shell
cd ~/ffmpeg_sources
curl -O -L https://archive.mozilla.org/pub/opus/opus-1.3.1.tar.gz
tar xzvf opus-1.3.1.tar.gz
cd opus-1.3.1
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
```


## libvpx

需要ffmpeg配置--enable-libvpx
```shell
cd ~/ffmpeg_sources
git clone --depth 1 https://github.com/webmproject/libvpx.git
cd libvpx
./configure --prefix="$HOME/ffmpeg_build" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm
make
make install
```


## Nvida驱动关联
```shell
git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git 
cd nv-codec-headers
make PREFIX="$HOME/ffmpeg_build" BINDDIR="$HOME/bin"
make install PREFIX="$HOME/ffmpeg_build" BINDDIR="$HOME/bin" 
```

## FFmpeg
```shell
cd ~/ffmpeg_sources
curl -O -L https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar xjvf ffmpeg-snapshot.tar.bz2
cd ffmpeg

# 不使用显卡编码
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" \
  --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
  --extra-libs=-lpthread \
  --extra-libs=-lm \
  --bindir="$HOME/bin" \
  --enable-gpl \
  --enable-libfdk_aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree

# 使用显卡编码 
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
--prefix="$HOME/ffmpeg_build" \
--pkg-config-flags="--static" \
--extra-cflags="-I$HOME/ffmpeg_build/include" \
--extra-ldflags="-L$HOME/ffmpeg_build/lib -L/usr/local/cuda/lib64" \
--extra-libs=-lpthread \
--extra-libs=-lm \
--bindir="$HOME/bin" \
--enable-gpl \
--enable-libfdk_aac \
--enable-libfreetype \
--enable-libmp3lame \
--enable-libopus \
--enable-libvpx \
--enable-libx264 \
--enable-libx265 \
--enable-cuda \
--enable-cuvid \
--enable-nvenc \
--enable-nonfree 

make 
make install
hash -d ffmpeg
```

Tip: 后续需要更新版本需要保留前面的安装内容.

------------------------------------------

## 更新依赖库

Development of FFmpeg is active and an occasional update can give you new features and bug fixes. First, remove the old files and then update the dependencies:
```shell
rm -rf ~/ffmpeg_build ~/bin/{ffmpeg,ffprobe,lame,x264,x265}
yum install autoconf automake bzip2 bzip2-devel cmake freetype-devel gcc gcc-c++ git libtool make mercurial pkgconfig zlib-devel
```

## Update x264
```shell
cd ~/ffmpeg_sources/x264
make distclean
git pull
```

Then run `./configure`, `make`, and `make install` as shown in the [Install x264](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#x264) section.

## Update x265
```shell
cd ~/ffmpeg_sources/x265_git
rm -rf ~/ffmpeg_sources/x265_git/build/linux/*
git pull
cd ~/ffmpeg_sources/x265_git/build/linux
```

Then run `cmake`, `make`, and `make install` as shown in the [Install x265](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#x265) section.

## Update libfdk_aac
```shell
cd ~/ffmpeg_sources/fdk_aac
make distclean
git pull
```

Then run `./configure`, `make`, and `make install` as shown in the [Install libfdk_aac](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#libfdk_aac) section.


## Update libvpx
```shell
cd ~/ffmpeg_sources/libvpx
make distclean
git pull
```

Then run `./configure`, `make`, and `make install` as shown in the [Install libvpx](http://trac.ffmpeg.org/wiki/CompilationGuide/Centos#libvpx) section.

## Update FFmpeg
```shell
rm -rf ~/ffmpeg_sources/ffmpeg

# 然后重新运行ffmpeg的安装
```

--------------------------------------
## 卸载ffmpeg及依赖库
```shell
rm -rf ~/ffmpeg_build ~/ffmpeg_sources ~/bin/{ffmpeg,ffprobe,lame,nasm,vsyasm,x264,yasm,ytasm}
yum erase autoconf automake bzip2 bzip2-devel cmake freetype-devel gcc gcc-c++ git libtool zlib-devel
hash -r
```



