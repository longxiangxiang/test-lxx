# ffmpeg常用命令

[返回上一级](index.md)

## MP4 转 h264
```shell
ffmpeg -i 2018.mp4 -codec copy -bsf: h264_mp4toannexb -f h264 tmp.264

注释：
-i 2018.mp4：  是输入的MP4文件
-codec copy： 从mp4中拷贝
-bsf: h264_mp4toannexb： 从mp4拷贝到annexB封装
-f h264： 采用h264格式
tmp.264： 输出的文件
```

## ffmpeg 解析RTSP视频流并间隔保存成视频文件、图片
```shell
ffmpeg -i rtsp://218.204.223.237:554/live/1/66251FC11353191F/e7ooqwcfbqjoo80j.sdp -r 1/60 -f image2 d:\\1\\images%05d.png -c copy -map 0 -f segment -segment_time 60 -segment_format mp4 "d:\\1\\out%03d.mp4"

注释：
-r 1/60：每分钟
-segment_time 60：：每分钟
```

## H264 Mp4转 H265 Mp4命令
```shell
ffmpeg -i e:\vrgf.mp4 -vcodec hevc -b:v 5000k -keyint_min 60 -g 60 -sc_threshold 0 e:\vrgf_compress1.mp4

注解：码率是5000k，编码是h265
　　　-keyint_min 60 -g 60 -sc_threshold 0
　　　其中-keyint_min为最小关键帧间隔，我这里设置为60帧；-sc_threshold这个命令会根据视频的运动场景，自动为你添加额外的I帧，所以会导致你编出来的视频关键帧间隔不是你设置的长度，这是只要将它设为0，问题就得到解决
```

## FFMPEG生成高质量的h264编码视频
```shell
FFMPEG 转换h264格式视频，为了使视频的每一秒都有关键帧，我们分两步来生成，第一步生成两个临时文件,再组合生成最终的视频文件(windows和linux系统都通用)
(1) ffmpeg -i input.mp4 -y input.temp.wav -s 640x360 -pix_fmt yuv420p -f rawvideo -y - | x264 --profile baseline --level 3 --keyint 25 --bitrate 512  --sar 1:1 --output input.temp.264 - 640x360
(2)ffmpeg -i input.temp.wav  -f h264 -i  input.temp.264  -y -vcodec copy -acodec libfaac -ab 50k output.mp4
  
这样生成的视频质量比较高，不会出现锯齿，也不会出现花屏
```

## ffmpeg截取视频中的一段视频
```shell
ffmpeg  -i test.avi -vcodec copy -acodec copy -ss 00:00:10 -to 00:00:50 test_cut.mp4 -y

上述命令会得到一个长为40秒的视频 test_cut.mp4
```

## 视频转换
```shell
# H264视频转ts视频流
ffmpeg -i test.h264 -vcodec copy -f mpegts test.ts

# H264视频转mp4
ffmpeg -i test.h264 -vcodec copy -f mp4 test.mp4

# ts视频转mp4
ffmpeg -i test.ts -acodec copy -vcodec copy -f mp4 test.mp4

# mp4视频转flv
ffmpeg -i test.mp4 -acodec copy -vcodec copy -f flv test.flv 
```

## 网络推送
```shell
# udp视频流的推送
ffmpeg -re  -i 1.ts  -c copy -f mpegts   udp://192.168.0.106:1234
```

## 视频拼接
```shell
# 裸码流的拼接，先拼接裸码流，再做容器的封装
ffmpeg -i "concat:test1.h264|test2.h264" -vcodec copy -f h264 out12.h264
```

## 视频转封装
```shell
ffmpeg -i input -vcodec copy output.mp4
```