# centos系统相关知识

[返回上一级](../index.md)

- [centos部署](centos-install.md)
- [服务器配置IP及BMC](static-ip.md)
- [重组raid操作](rebuild-raid5.md)
- [使用nfs服务器做nas](create-nas.md)
- [chrony同步服务器时间](chrony.md)
- [查看服务器信息](get-msg.md)
- [磁盘加密](disk-passwd.md)