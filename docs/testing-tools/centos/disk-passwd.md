# 磁盘加密

[返回上一级](index.md)

## 安装cryptsetup
> 需要自己下载好安装包安装
```shell
yum -y install cryptsetup

# 离线包下载方法
yum -y install cryptsetup --downloadonly --downloaddir=./
```

## 加密数据盘
```shell
cryptsetup luksFormat /dev/sdb

WARNING!
========
这将覆盖 /dev/sdb 上的数据，该动作不可取消。
Are you sure? (Type uppercase yes): YES # 注意这里必须是大写的YES
输入密码：
确认密码：
```

## 生成随机key file
> 任意方式生成都可以，随机就行，举例：
```shell
dd if=/dev/sdb of=/root/enc.key bs=1 count=4096
```

## 添加 key file 作为密码之一
```shell
cryptsetup luksAddKey /dev/sdb /root/enc.key
# 输入磁盘的密码：
```

## 分区映射
```shell
cryptsetup luksOpen -d /root/enc.key /dev/sdb sc_data
# 会自动在/dev/mapper/目录下生成一个文件sc_data
```

## 创建文件系统
```shell
mkfs.xfs /dev/mapper/sc_data
# 此次的sc_data就是前一步的sc_data
```

## 挂载
```shell
mkdir /admin
vim /etc/fstab
# 新增行
# /dev/mapper/sc_data /admin xfs defaults 0 0
mount -a
```

## 修改开机启动配置
```shell
vim /etc/crypttab
sc_data /dev/sdb /root/enc.key
# enc.key就是之前生成的key file
```

## 脚本

[disk-passwd.sh](../attachments/disk-passwd.sh)

## 验证方式
> 1. 验证重启服务器后是否需要鉴权 <br/>
>> 重启服务器，不需要密码可以正常进入centos <br/>
>> 把/root/enc.key移走，重启服务器，启动后需要输入密码后可以正常进入centos（测试完毕需要把文件移回去原来目录）<br/>
> 2. 验证打开磁盘是否需要密码 <br/>
>> 服务器关机，把硬盘拆下来，接到电脑上，这里介绍的是window系统的方法 <br/>
>> 在电脑上安装UFS Explorer Professional Recovery <br/>
>> 按下图步骤，输入密码后才能打开磁盘 <br/>

![img001](../attachments/disk-passwd/img001.png)

![img002](../attachments/disk-passwd/img002.png)

![img003](../attachments/disk-passwd/img003.png)

![img004](../attachments/disk-passwd/img004.png)