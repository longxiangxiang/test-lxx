# 查看服务器信息

[返回上一级](index.md)

## 查看系统内核以及centos版本
```shell
# 查看CentOS系统的版本
cat /etc/*-release

# 查看内核版本
uname -a
```

## 查看cpu
```shell
# 查看cpu相关信息
lscpu

# 查看cpu型号
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c

# 查看物理cpu个数
cat /proc/cpuinfo | grep "physical id" | sort | uniq|wc -l

# 查看逻辑cpu的个数
cat /proc/cpuinfo | grep "processor" |wc -l

# 查看cpu是几核
cat /proc/cpuinfo | grep "cores"|uniq

# 查看cpu使用率
top
## VIRT：virtual memory usage 虚拟内存
## RES：resident memory usage 常驻内
## SHR：shared memory 共享内存

# 查看系统各服务cpu、memory占用情况
kubectl describe nodes controller-148   # controller-148为节点名称
```

## 查看内存
```shell
# 查看操作系统版本
cat /etc/redhat-release

# 查看内存：以G为单位
free -g

# 查看内存：以M为单位
free -m

## Mem:内存的使用情况总览表。
## totel:机器总的物理内存
## used：用掉的内存。
## free:空闲的物理内存。
```

## 查看磁盘io
```shell
# 需要用到iostat，若没有，可以使用命令部署下历来
yum install sysstat

# 查看磁盘io
iostat -d -k -x 1 10
## rrqm/s: 每秒进行merge的读操作数目。即delta(rmerge/s)  
## wrqm/s: 每秒进行merge的写操作数目。即delta(wmerge/s)     
## r/s: 每秒完成的读I/O设备次数。即delta(rio)/s
## w/s: 每秒完成的写I/O设备次数。即delta(wio)/s
## rsec/s：每秒读扇区数。即delta(rsec)/s
## wsec/s：每秒写扇区数。即delta(wsec)/s
## rkB/s: 每秒读的kb字节数，是rsec/s的一般，因为每扇区大小512字节
## wkB/s: 每秒写的kb字节数  
## avgrq-sz: 平均每秒设备I/O操作的数据大小(扇区)。即delta(rsec+wsec)/delta(rio+wio) 
## avgqu-sz: 平均I/O队列长度。即delta(aveq)/s/1000(因为aveq单位为毫秒)  
## await: 平均每次设备I/O操作的等待时间(毫秒)。即delta(ruse+wuse)/delta(rio+wio) 
## r_await：平均每次设备I/O读操作的等待时间(毫秒)。即delta(ruse)/delta(rio)
## w_await：平均每次设备I/O写操作的等待时间(毫秒)。即delta(wuse)/delta(wio)
## svctm：平均每次设备I/O操作的服务时间(毫秒)。即delta(use)/delta(rio+wio) 
## %util：一秒中有百分之多少的时间用于I/O操作，或者说一秒中有多少时间I/O队列是非空的
## 如果%util接近100%,表明I/O请求太多,I/O系统已经满负荷，磁盘可能存在瓶颈,一般%util大于70%，I/O压力就比较大，读取速度有较多的wait，然后再看其他的参数

# 找出io高的进程的工具：iotop
yum install -y iotop    # 若没有可以使用命令安装

iotop

# 查看端口占用情况：lsof
yum install -y lsof      # 若没有可以使用命令安装

## 列出占用端口情况
lsof -i
## 例子：列出80端口占用情况
lsof -i:80
```

## 查看带宽
```shell
# 使用iftop工具，若没有使用下面的命令安装
yum install -y iftop

# 查看带宽
iftop -P
## TX: 从网卡发出的流量
## RX: 网卡接收的流量
## <= 与 =>: 表示流量的方向
## TOTAL: 网卡发送接收总流量
## cum: iftop开始运行到当前时间点的总流量
## peak: 网卡流量峰值
## rates: 分别表示最近2s、10s、40s的平均流量
```

## 查看gpu
```shell
# 查看gpu型号
lspci | grep -i nvidia

# 查看gpu总个数
lspci | grep -i nvidia|wc -l

# 查看gpu使用情况
nvidia-smi

## nvidia-smi 命令的其他参数
nvidia-smi -L                   # 显示连接到系统的 GPU 列表,可以查询到gpu的uuid
nvidia-smi -i 0                 # 参数指定某个GPU的编号，多用于查看 GPU 信息时指定其中一个 GPU
nvidia-smi -i 0 -q              # 查看 GPU 的全部信息。可通过 -i 参数指定查看某个 GPU 的参数

# 查看已使用的gpu及占用的进程pid
nvidia-smi -q|grep "Process ID"

# 查看已使用的gpu卡个数
nvidia-smi -q|grep "Process ID"|wc -l

# 方舟查看各节点gpu已使用个数
cd /data/awesome/viper/
ansible nodes -m shell -a "nvidia-smi -q|grep 'Process ID'|wc -l"
```
![img002](../attachments/img002.png)