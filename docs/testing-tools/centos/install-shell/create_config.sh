#!/bin/bash

NODE_1_IP=""
NODE_2_IP=""
NODE_3_IP=""
ARCH="amd64"
CPU_TYPE="intelxeon"
SSH_PORT=22
NETWORK_NAME="eth100"
BDP_FLAG="true"
BDP_FUSION="true"
SERVER_TYPE="senseFoundry"
MULTIVIEW_ENABLED="true"
VIPER_OFFLINE_ENABLED="true"
VIPER_IP=""
VIPER_OFFLINE_IP=""
NODE_1_USER="root"
NODE_1_PASSWD="V1p3r1@#$%"
NODE_1_DEV="sdb"
NODE_2_USER="root"
NODE_2_PASSWD="V1p3r1@#$%"
NODE_2_DEV="sdb"
NODE_3_USER="root"
NODE_3_PASSWD="V1p3r1@#$%"
NODE_3_DEV="sdb"

WORK_DIR="/data"

function Usage() {
    echo "./create_config.sh"
    echo "    --node_1_ip: node-1节点IP信息"
    echo "    --node_2_ip: node-2节点IP信息"
    echo "    --node_3_ip: node-3节点IP信息"
    echo "    --arch: device arch [amd64, arm64], default: amd64"
    echo "    --cpu_type: cpu类型：support 'intelxeon' or 'hygon' or 'kunpeng920'"
    echo "    --ssh_port: ssh连接端口，默认22"
    echo "    --network_name: 网卡名称，所有机器需要一致,默认eth100"
    echo "    --bdp_flag: 大数据标签，默认true"
    echo "    --bdp_fusion: 融合大数据标签，默认true"
    echo "    --server_type: 对接的底层类型,'senseFoundry' or 'senseUnity' or 'senseFairy'"
    echo "    --multiview_enabled: 离线解析标签，默认true"
    echo "    --viper_offline_enabled: 融合离线解析标签，默认true"
    echo "    --viper_ip: 对接的底层ip"
    echo "    --viper_offline_ip: 对接的底层离线解析节点ip"
    echo "    --node_1_user: node-1节点ssh用户"
    echo "    --node_1_passwd: node-1节点ssh密码"
    echo "    --node_1_dev: node-1节点数据挂载盘符名称"
    echo "    --node_2_user: node-2节点ssh用户"
    echo "    --node_2_passwd: node-2节点ssh密码"
    echo "    --node_2_dev: node-2节点数据挂载盘符名称"
    echo "    --node_3_user: node-3节点ssh用户"
    echo "    --node_3_passwd: node-3节点ssh密码"
    echo "    --node_3_dev: node-3节点数据挂载盘符名称"
}

# 获取当前脚本的路径
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

echo "当前脚本路径：${SCRIPT_DIR}"


function install_sshpass()
{
        cmd="sshpass"
        if [ -z "$(command -v $cmd)" ]; then
                echo "command not install"
                yum install $cmd -y
        else
                $cmd -V
        fi
}


function create_config(){
  # 从模板拷贝一份过来
  cp -fr "${SCRIPT_DIR}"/config-template.yaml ${WORK_DIR}/config.yaml

  # 确保sshpass依赖有部署
  command -v sshpass >/dev/null 2>&1 || { echo >&2 "sshpass is not installed. Installing it now...";
  install_sshpass; }

  # 替换config.yaml中需要修改的配置项
  sed -i "s/{node_1_ip}/${NODE_1_IP}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{arch}/${ARCH}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{cpu_type}/${CPU_TYPE}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{ssh_port}/${SSH_PORT}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{network_name}/${NETWORK_NAME}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{bdp_flag}/${BDP_FLAG}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{bdp_fusion}/${BDP_FUSION}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{server_type}/${SERVER_TYPE}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{multiview_enabled}/${MULTIVIEW_ENABLED}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{viper_offline_enabled}/${VIPER_OFFLINE_ENABLED}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{viper_ip}/${VIPER_IP}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{viper_offline_ip}/${VIPER_OFFLINE_IP}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{node_1_user}/${NODE_1_USER}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{node_1_passwd}/${NODE_1_PASSWD}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{node_1_dev}/${NODE_1_DEV}/g" ${WORK_DIR}/config.yaml
  sed -i "s/{bdp_master_ip}/${VIPER_IP}/g" ${WORK_DIR}/config.yaml

  if [ "${ARCH}" == "amd64" ]; then
    HTTPS_SIGN_CATEGORY="standard"
    sed -i "s/{https_sign_category}/${HTTPS_SIGN_CATEGORY}/g" ${WORK_DIR}/config.yaml
  elif [ "${ARCH}" == "arm64" ]; then
    HTTPS_SIGN_CATEGORY="standard-arm"
    sed -i "s/{https_sign_category}/${HTTPS_SIGN_CATEGORY}/g" ${WORK_DIR}/config.yaml
  else
    echo "Unsupported architecture: ${ARCH}"
    exit 1
  fi

  NODE_1_END_SIZE=$(($(lsblk -b | grep "^${NODE_1_DEV}" | awk '{print $4}') >> 20))
  sed -i "s/{node_1_end_size}/${NODE_1_END_SIZE}/g" /data/config.yaml

  if [ "${NODE_2_IP}" == ""  ];then
    sed -i "/host: node-2/,+4d" ${WORK_DIR}/config.yaml
    sed -i "/node-2:/,+8d" ${WORK_DIR}/config.yaml
  else
    sed -i "s/{node_2_ip}/${NODE_2_IP}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_2_user}/${NODE_2_USER}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_2_passwd}/${NODE_2_PASSWD}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_2_dev}/${NODE_2_DEV}/g" ${WORK_DIR}/config.yaml
    NODE_2_END_SIZE=$(sshpass -p ${NODE_2_PASSWD} ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${NODE_2_USER}@${NODE_2_IP} "lsblk -b|grep '^${NODE_2_DEV}'" | awk '{print $4/1024/1024}')
    sed -i "s/{node_2_end_size}/${NODE_2_END_SIZE}/g" /data/config.yaml
  fi

  if [ "${NODE_3_IP}" == ""  ];then
    sed -i "/host: node-3/,+4d" ${WORK_DIR}/config.yaml
    sed -i "/node-3:/,+8d" ${WORK_DIR}/config.yaml
  else
    sed -i "s/{node_3_ip}/${NODE_3_IP}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_3_user}/${NODE_3_USER}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_3_passwd}/${NODE_3_PASSWD}/g" ${WORK_DIR}/config.yaml
    sed -i "s/{node_3_dev}/${NODE_3_DEV}/g" ${WORK_DIR}/config.yaml
    NODE_3_END_SIZE=$(sshpass -p ${NODE_3_PASSWD} ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${NODE_3_USER}@${NODE_3_IP} "lsblk -b|grep '^${NODE_3_DEV}'" | awk '{print $4/1024/1024}')
    sed -i "s/{node_3_end_size}/${NODE_3_END_SIZE}/g" /data/config.yaml
  fi
}

function main() {
    while [[ $# -gt 0 ]]; do
       case "$1" in
          --node_1_ip) # master本机IP信息
              NODE_1_IP="$2"
              shift
              shift
              ;;
          --arch)  # cpu架构:arm64，amd64，不填默认为：amd64
              ARCH="$2"
              shift
              shift
              ;;
          --cpu_type) # cpu类型：support "intelxeon" or "hygon" or "kunpeng920"
            CPU_TYPE="$2"
              shift
              shift
              ;;
          --ssh_port) # ssh连接端口，默认22
            SSH_PORT="$2"
              shift
              shift
              ;;
          --network_name) # 网卡名称，所有机器需要一致
            NETWORK_NAME="$2"
              shift
              shift
              ;;
          --bdp_flag)
            BDP_FLAG="$2"
              shift
              shift
              ;;
          --bdp_fusion)
            BDP_FUSION="$2"
              shift
              shift
              ;;
          --server_type) # 对接的底层类型,"senseFoundry" or "senseUnity" or "senseFairy"
              SERVER_TYPE="$2"
              shift
              shift
              ;;
          --multiview_enabled)
            MULTIVIEW_ENABLED="$2"
              shift
              shift
              ;;
          --viper_offline_enabled)
            VIPER_OFFLINE_ENABLED="$2"
              shift
              shift
              ;;
          --viper_ip)
            VIPER_IP="$2"
              shift
              shift
              ;;
          --viper_offline_ip)
            VIPER_OFFLINE_IP="$2"
              shift
              shift
              ;;
          --node_1_user)
            NODE_1_USER="$2"
              shift
              shift
              ;;
          --node_1_passwd)
            NODE_1_PASSWD="$2"
              shift
              shift
              ;;
          --node_1_dev)
            NODE_1_DEV="$2"
              shift
              shift
              ;;
          --node_2_ip) # node-2节点IP信息
              NODE_2_IP="$2"
              shift
              shift
              ;;
          --node_2_user)
            NODE_2_USER="$2"
              shift
              shift
              ;;
          --node_2_passwd)
            NODE_2_PASSWD="$2"
              shift
              shift
              ;;
          --node_2_dev)
            NODE_2_DEV="$2"
              shift
              shift
              ;;
          --node_3_ip) # node-3节点IP信息
              NODE_3_IP="$2"
              shift
              shift
              ;;
          --node_3_user)
            NODE_3_USER="$2"
              shift
              shift
              ;;
          --node_3_passwd)
            NODE_3_PASSWD="$2"
              shift
              shift
              ;;
          --node_3_dev)
            NODE_3_DEV="$2"
              shift
              shift
              ;;
          *)
              Usage;
              echo "1" > /tmp/create_config.log  # sep平台成功/失败标志位判断文件
              exit 1;
        esac
    done

    if [[ ${NODE_2_IP} == "" ]];then
        NODE_2_IP=""
    elif [[ ${NODE_3_IP} == "" ]];then
        NODE_3_IP=""
    fi

    echo -e "\033[32m ***********************************************start get packages*********************************************** \033[0m"

    # 生成配置文件
    create_config;
}

main "$@"
echo $? > /tmp/create_config.log