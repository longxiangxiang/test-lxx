#!/bin/bash

DISK="/sdb"
FILE="/root/disk-parted-rules-amd64.yml"


function print_usage() {
    echo "./park_disk.sh"
    echo "    --disk: disk name, default: /sdb"
    echo "    --file: 分区文件,默认disk-parted-rules-amd64.yml"
}

function install_yq()
{
        cmd="yq"
        if [ -z "$(command -v $cmd)" ]; then
            echo "command not install"
            wget -N -P /tmp/tools http://sz.viper.sensesecurity.net/infra/helm/yq-4.21.1-amd64
            cd /tmp/tools && cp yq-4.21.1-amd64 /usr/local/bin && chmod +x /usr/local/bin/yq-4.21.1-amd64
            rm -rf /usr/local/bin/yq
            ln -s /usr/local/bin/yq-4.21.1-amd64 /usr/local/bin/yq
        else
                $cmd --version
        fi
}

function check_disk() {
    DISK_ORDER=$(find /dev/disk/by-path/ -type l -exec ls -l {} \; | grep '>' | grep -v part | sort -k9 | awk '{print $NF}' | uniq | awk -F '/' '{print $NF}')
    ACTUAL_ORDER=$(lsblk -dno NAME | sort)

    if [ "$DISK_ORDER" == "$ACTUAL_ORDER" ]; then
        echo "Disk order is correct."
        echo 0 > /tmp/check_disk.log
    else
        echo "Invalid disk order detected!"
        echo 1 > /tmp/check_disk.log
    fi


    if mount | grep -q '/locals'; then
        echo "/locals 已经挂载"
        echo 1 > /tmp/check_disk.log
    else
        echo "/locals 未挂载"
    fi
}

function part_disk() {
    # 检查是否有/locals已经挂载
    if mount | grep -q '/locals'; then
        echo "/locals 已经挂载"
        exit 0
    fi

    # 执行分区命令
    parted "/dev/${DISK}" <<EOF
p
mklabel gpt
mkpart primary 0% 100%
quit
EOF

    # 格式化分区
    mkfs.xfs -f /dev/"${DISK}"1

    # 挂载分区到指定目录
    sudo mkdir /locals
    sudo mount /dev/"${DISK}"1 /locals

    # 将挂载信息写入fstab文件
    uuid=$(blkid -s UUID -o value /dev/"${DISK}"1)
    echo "UUID=$uuid /locals  xfs  defaults,noatime  0  0" | sudo tee -a /etc/fstab

    echo "分区完成，挂载信息已写入fstab文件"
}

function create_dir() {
    # 先检查yq依赖
    install_yq

    # 获取数据盘分配规则文件
    if [ ! -f "${FILE}" ];then
      echo "parted rule file not found,please give me the file"
      exit 1 > /tmp/sc_depoy.log
    else
      file="${FILE}"
    fi

    # 读取yml文件中centurio-business节点的volume_names值,注意,不同版本可能配置文件中参数名称不一样,需要手动确认
    volume_names=$(yq e '.default_bmount_disk_parted_rules[]|select(.name=="centurio-business")|.volume_names[]' $file)

    # 将volume_names的值写入到list中
    list=()
    for name in $volume_names; do
        list+=("$name")
    done

    for i in "${list[@]}"; do
        mkdir -p /locals/"$i" && mkdir -p /mnt/locals/"$i"
        echo -e "/locals/$i   /mnt/locals/$i   none    bind    0 0" >> /etc/fstab
    done

    # 所有配置写入 `/etc/fstab` 后执行 `mount -a`
    mount -a
}


function main() {
    while [[ $# -gt 0 ]]
    do
        key="$1"
        case $key in
            check)
                ACTION="check_disk"
                shift
                ;;

            part)
                ACTION="part_disk"
                shift
                ;;

            dir)
                ACTION="create_dir"
                shift
                ;;

            all)
                ACTION="all"
                shift
                ;;

            --disk|-d)
                DISK="$2"
                shift
                shift
                ;;

            --file|-f)
                FILE="$2"
                shift
                shift
                ;;
            *)
                print_usage;
                exit 1;
        esac
    done

    if [ "${ACTION}" == "part_disk" ]; then
        part_disk
    elif [ "${ACTION}" == "create_dir" ]; then
        create_dir
    elif [ "${ACTION}" == "check_disk" ]; then
        check_disk
    elif [ "${ACTION}" == "all" ]; then
        part_disk
        create_dir
    else
        print_usage;
        exit 1
    fi
}

main "$@"
echo $? > /tmp/part_disk.log