#!/bin/bash

cooldown_time=30
log_dir="/tmp/sc_deploy.log"

function Usage() {
    echo "./check_deploy.sh"
    echo "    --cooldown_time: 检测间隔时间, default: 30"
    echo "    --log_dir: 检测日志, default: /tmp/sc_deploy.log"
}

function check_infra() {
    while true; do
        if [ "$(pgrep -cf 'ansible-playbook infra.yml')" -eq 0 ]; then
            echo "ansible-playbook infra.yml 进程已结束，检测infra状态"
            # 校验infra部署是否成功--nodes是否ready
            ret=$(kubectl get nodes --no-headers|grep -vc Ready)
            if [ "$ret" -ne 0 ]; then
               echo "!!!ERROR: deploy_infra failed! check ${WORK_DIR}/install_infra.log now stop.."
               exit 1 > /tmp/check_deploy.log
            fi
            echo "#### install infra success ####"
            break
        else
          echo "ansible-playbook infra.yml 进程存在，等待${cooldown_time}秒后再次检测..."
          sleep "${cooldown_time}"
        fi
    done
}

function check_product() {
    while true; do
        if [ "$(pgrep -cf 'line-deploy.sh')" -eq 0 ]; then
            echo "product 进程已结束，检测服务状态"
            if [ -f "${log_dir}" ]; then
                if [ "$(cat ${log_dir})" -eq 0 ]; then
                    echo "#### product success ####"
                    echo "0" > /tmp/check_deploy.log
                else
                    echo "#### product failed ####"
                    exit 1 > /tmp/check_deploy.log
                fi
            fi
            break
        else
            echo "product 进程存在，等待${cooldown_time}秒后再次检测..."
            sleep "${cooldown_time}"
        fi
    done
}

function main() {
    while [[ $# -gt 0 ]]; do
       case "$1" in
          --cooldown_time) # 检测进程状态间隔时间
              cooldown_time="$2"
              shift
              shift
              ;;
          --log_dir)  # product进程成功的标志目录
              log_dir="$2"
              shift
              shift
              ;;
          *)
              Usage;
              echo "1" > /tmp/check_deploy.log  # sep平台成功/失败标志位判断文件
              exit 1;
        esac
    done

    # 检测部署进程状态
    check_infra
    check_product
}

main "$@"
echo $? > /tmp/check_deploy.log
