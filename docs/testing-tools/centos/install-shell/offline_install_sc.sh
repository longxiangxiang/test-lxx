#!/bin/bash

WORK_DIR="/data"
PRODUCT_NAME="sensecenturio"
SHELL_PATH=$(pwd)
retry_count=0
NODE_1_DEV="sdb"
NODE_2_DEV="sdb"
NODE_3_DEV="sdb"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function print_usage() {
    echo ""
    echo "usage: bash ./offline_install_sc.sh [COMMAND] [OPTIONS]"
    echo ""
    echo "prepare execute:"
    echo "./offline_install_sc.sh prepare -p [yum|registry|model|ansible|chart]"
    echo ""
    echo "genconfig execute:"
    echo "./offline_install_sc.sh genconfig"
    echo ""
    echo "deploy infra execute:"
    echo "                     ./offline_install_sc.sh deploy -s infra"
    echo ""
    echo "deploy product execute:"
    echo "                    ./offline_install_sc.sh deploy -s product"
    echo ""
    echo "  COMMAND:"
    echo "      prepare                   prepare packages before deploymnet"
    echo "      genconfig                 generate env.yaml & inventory & all.yml"
    echo "      deploy                    deploy infra.yml and product.yml playbooks"
    echo "      all                       combine 'prepare' & 'deploy' & 'check' "
    echo ""
    echo "  OPTIONS:"
    echo "      -p,  --package            option for prepare packages include yum/registry/model/applet/ansible/chart"
    echo "      -c,  --clean              option for prepare command to clean all packages"
    echo "      -s,  --step               option for deploy command to execute infra and product playbook step by step"
    echo "    --product_name: product name, default: sensecenturio"
    echo ""
}

##########################
###### 获取部署包名称 ######
##########################
function get_pkgs_name() {
    YUM_NAME=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*yum*.tar*" -exec basename {} \; -quit)
    YUM_MD5=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*yum*.md5*" -exec basename {} \; -quit)
    REGISTRY_NAME=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*registry*.tar*" -exec basename {} \; -quit)
    REGISTRY_MD5=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*registry*.md5*" -exec basename {} \; -quit)
    ANSIBLE_NAME=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*ansible*.rpm*" -exec basename {} \; -quit)
    ANSIBLE_MD5=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*ansible*.md5*" -exec basename {} \; -quit)
    CHARTS_NAME=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*chart*.tar*" -exec basename {} \; -quit)
    CHARTS_MD5=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*chart*.md5*" -exec basename {} \; -quit)
    ATTACHMENTS_NAME=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "*attachements*.tar*" -exec basename {} \; -quit)
    if [ -z "${YUM_NAME}" ] || [ -z "${REGISTRY_NAME}" ] || [ -z "${ANSIBLE_NAME}" ] || [ -z "${CHARTS_NAME}" ] || [ -z "${ATTACHMENTS_NAME}" ]; then
        echo "Error: one or more packages not found in ${WORK_DIR}"
        exit 1
    fi
}

###############################
###### 使用tmux确保正常部署 ######
###############################
function check_tmux() {
    # 确保tmux依赖有部署
    command -v tmux >/dev/null 2>&1 || { echo >&2 "tmux is not installed. Installing it now..."; yum install -y tmux; }

    if [ "$(tmux ls | grep -c install_offline)" -eq 0 ];then
        tmux new-session -d -s install_offline
    fi
}

################################
###### 初始化部署部署包--yum ######
################################
function prepare_yum() {
    echo "###### install infra yum ######"
    if [ -f "${WORK_DIR}/${YUM_NAME}" ]; then
        if [ -f "${WORK_DIR}/${YUM_MD5}" ]; then
            pushd ${WORK_DIR} || exit 1
            # check md5
            md5sum -c "${YUM_MD5}"
            popd  || exit 1
	      fi

        pushd ${WORK_DIR} || exit 1
        # 解压压缩包
        tar zxvf "${YUM_NAME}"
        popd  || exit 1

        pushd ${WORK_DIR}/infra-distribution-yum || exit 1
        # 部署yum
        ./install.sh
        popd  || exit 1
    else
        echo "${WORK_DIR}/${YUM_NAME} is not exist"
        exit 1 > /tmp/sc_prepare.log
    fi

    # 部署依赖
    yum install vim net-tools tmux -y

    # 检测yum是否部署成功
    while true;do
      yum_status=$(netstat -lntp | grep -c 6000)
      if [ "${yum_status}" -gt 0 ]; then
        echo "###### install infra yum succeed #####"
        echo 0 > /tmp/sc_prepare.log
        break
      elif [ ${retry_count} -ge 3 ];then
        echo "#### install infra yum failed after ${retry_count} retries ####"
        exit 1 > /tmp/sc_prepare.log
      else
        echo "##### install infra yum failed, retrying ...... #####"
        sleep 5
        retry_count=$((retry_count + 1))
      fi
    done
}

#####################################
###### 初始化部署部署包--registry ######
#####################################
function prepare_registry() {
    echo "###### install infra registry ######"
    if [ -f "${WORK_DIR}/${REGISTRY_NAME}" ]; then
        if [ -f "${WORK_DIR}/${REGISTRY_MD5}" ]; then
            pushd ${WORK_DIR} || exit 1
            # check md5
            md5sum -c "${REGISTRY_MD5}"
            popd  || exit 1
        fi

        pushd ${WORK_DIR} || exit 1
        # 解压registry包
        tar zxvf "${REGISTRY_NAME}"
        popd  || exit 1

        pushd ${WORK_DIR}/infra-distribution-registry || exit 1
        # 部署registry
        ./install.sh
        popd || exit 1
    else
        echo "${REGISTRY_NAME} is not exist"
        exit 1 > /tmp/sc_prepare.log
    fi

    # 检测registry是否部署成功
    while true;do
      registry_status=$(netstat -lntp | grep -c 5000)
      if [ "${registry_status}" -gt 0 ]; then
        echo "###### install infra registry succeed #####"
        echo 0 > /tmp/sc_prepare.log
        break
      elif [ ${retry_count} -ge 3 ];then
        echo "#### install infra registry failed after ${retry_count} retries ####"
        exit 1 > /tmp/sc_prepare.log
      else
        echo "##### install infra registry failed, retrying ...... #####"
        sleep 5
        retry_count=$((retry_count + 1))
      fi
    done
}

####################################
###### 初始化部署部署包--ansible ######
####################################
function prepare_ansible() {
    echo "###### install ansible tool ######"
    if [ -f "${WORK_DIR}/${ANSIBLE_NAME}" ]; then
        if [ -f "${WORK_DIR}/${ANSIBLE_MD5}" ]; then
            pushd ${WORK_DIR} || exit 1
            # check md5
            md5sum -c "${ANSIBLE_MD5}"
            popd || exit 1
        fi

        pushd ${WORK_DIR} || exit 1
        # 部署ansible
        yum localinstall -y "${ANSIBLE_NAME}"
        infra-cli init
        popd || exit 1

    else
        echo "${ANSIBLE_NAME} is not exist"
        exit 1 > /tmp/sc_prepare.log
    fi

    # 检测ansible是否部署成功
    while true;do
      ansible_status=$(rpm -qa |grep -c sensecenturio)
      if [ "${ansible_status}" -gt 0 ]; then
        echo "###### install infra ansible succeed #####"
        echo 0 > /tmp/sc_prepare.log
        break
      elif [ ${retry_count} -ge 3 ];then
        echo "#### install infra ansible failed after ${retry_count} retries ####"
        exit 1 > /tmp/sc_prepare.log
      else
        echo "##### install infra ansible failed, retrying ...... #####"
        sleep 5
        retry_count=$((retry_count + 1))
      fi
    done
}

##################################
###### 初始化部署部署包--chart ######
##################################
function prepare_chart() {
    echo "###### install chart package ######"
    if [ ! -d ${WORK_DIR}/awesome ]; then
        mkdir -p ${WORK_DIR}/awesome
    fi

    if [ -f "${WORK_DIR}/${CHARTS_NAME}" ]; then
        if [ -f "${WORK_DIR}/${CHARTS_MD5}" ]; then
            pushd ${WORK_DIR} || exit 1
            # check md5
            md5sum -c "${CHARTS_MD5}"
            popd || exit 1
        fi

        pushd ${WORK_DIR} || exit 1
        # 解压chart包到指定目录
        tar zxvf "${CHARTS_NAME}" -C ${WORK_DIR}/awesome
        popd || exit 1

    else
        echo "${CHARTS_NAME} is not exist"
        exit 1 > /tmp/sc_prepare.log
    fi

    if [ -d ${WORK_DIR}/awesome/${PRODUCT_NAME}  ]; then
        echo "###### install chart package succeed #####"
        echo 0 > /tmp/sc_prepare.log
    else
        echo "##### install chart package failed #####"
        exit 1 > /tmp/sc_prepare.log
    fi

    pushd ${WORK_DIR}/awesome/${PRODUCT_NAME}  || exit 1
      if [ -d ansible-infra ]; then
          rm -rf ansible-infra
      fi
      # 初始化chart
      ./scripts/offline-deploy.sh prepare
    popd  || exit 1

}

################################
###### 初始化部署部署包--清理 ######
################################
function prepare_clean_package() {
    pushd ${WORK_DIR} || exit 1
      if [ -f "${YUM_NAME}" ]; then
        echo "##### clean yum package #####"
        rm -rf "${YUM_NAME}"
      elif [ -f "${REGISTRY_NAME}" ]; then
        echo "##### clean registry package #####"
        rm -rf "${REGISTRY_NAME}"
      elif [ -f "${ANSIBLE_NAME}" ]; then
        echo "##### clean ansible package #####"
        rm -rf "${ANSIBLE_NAME}"
      elif [ -f "${CHARTS_NAME}" ]; then
        echo "##### clean chart package #####"
        rm -rf "${CHARTS_NAME}"
      fi
    popd  || exit 1
}

################################
###### 初始化部署部署包--全部 ######
################################
function prepare_packages() {
    if [ ! -d ${WORK_DIR} ]; then
        mkdir ${WORK_DIR}
    fi

    if [ -n "${OPT_PACKAGE}" ]; then
        case ${OPT_PACKAGE} in
            yum)
                prepare_yum
                ;;
            registry)
                prepare_registry
                ;;
            ansible)
                prepare_ansible
                ;;
            chart)
                prepare_chart
                ;;
            *)
                print_usage;
                exit 1;
                ;;
        esac
    else
        if [ "${OPT_CLEAN}" == "yes" ]; then
            prepare_clean_package
        else
            prepare_yum
            prepare_registry
            prepare_ansible
            prepare_chart
        fi
    fi
}

########################
###### 渲染配置文件 ######
########################
function generate_config() {
    config_count=$(find "${WORK_DIR}" -maxdepth 1 -type f -name "config.yaml" | wc -l)
    if [ "${config_count}" -gt 0 ]; then
        echo "found config.yaml in ${WORK_DIR}"
        pushd ${WORK_DIR}/awesome/${PRODUCT_NAME}  || exit 1
            # 将配置文件拷贝到指定目录
            \cp -f ${WORK_DIR}/config.yaml ${WORK_DIR}/awesome/${PRODUCT_NAME}/gen/config.yml
            # 生成配置文件
            bash scripts/offline-deploy.sh genconfig
            \cp -f ${WORK_DIR}/awesome/${PRODUCT_NAME}/gen/output/env.yaml env.yaml
            \cp -f ${WORK_DIR}/awesome/${PRODUCT_NAME}/gen/output/group_vars/all.yml ${WORK_DIR}/awesome/viper/group_vars/all.yml
            \cp -f ${WORK_DIR}/awesome/${PRODUCT_NAME}/gen/output/inventory.yaml ${WORK_DIR}/awesome/viper/inventory
        popd  || exit 1
    else
        echo "not found config.yaml in ${WORK_DIR}"
        exit 1 > /tmp/sc_genconfig.log
    fi

    if [ -f "${WORK_DIR}/awesome/viper/inventory" ]; then
        echo "###### generate inventory succeed ######"
        echo 0 > /tmp/sc_genconfig.log
    else
        echo "###### generate inventory failed ######"
        exit 1 > /tmp/sc_genconfig.log
    fi

    if [ -f "${WORK_DIR}/awesome/viper/group_vars/all.yml" ]; then
        echo "###### generate all.yml succeed ######"
        echo 0 > /tmp/sc_genconfig.log
    else
        echo "###### generate all.yml failed ######"
        exit 1 > /tmp/sc_genconfig.log
    fi
}


######################################
###### 将部署步骤写入配置使其开机执行 ######
######################################
function edit_rc(){
  chmod +x /etc/rc.d/rc.local
  cp -rf "${DIR}"/centurio_deploy_offline/offline_install_sc.sh ${WORK_DIR}/offline_install_sc.sh
  echo "export HOME=/root" | tee -a /etc/rc.d/rc.local
  echo "nohup /bin/bash -x ${WORK_DIR}/offline_install_sc.sh deploy &> >(tee -a ${WORK_DIR}/sc_deploy.log) &" >>/etc/rc.d/rc.local
  echo "exit 0" >>/etc/rc.d/rc.local
}

####################################
###### 检测磁盘顺序，防止部署失败  ######
####################################
function check_disk() {
    pushd ${WORK_DIR}/awesome/viper   || exit 1
	  # 拷贝脚本到各个节点
    ansible nodes -m copy -a "src=${DIR}/part_disk.sh dest=/root/part_disk.sh mode=0755"
	  # 执行脚本检查磁盘
	  ansible nodes -m shell -a "bash -x part_disk.sh check"

	  # 检查结果，如果有一个节点的返回码不为0，则手动分区，否则输出0
    result=$(ansible nodes -m shell -a "cat /tmp/check_disk.log" | grep -v rc)
	  if [[ "${result}" =~ "1" ]]; then
	      echo "Invalid disk order detected!"
		    echo "start to auto park disk"
		    parted_rule="/usr/local/infra/ansible/roles/infra-defaults/defaults/disk-parted-rules-amd64.yml"
		    ansible nodes -m copy -a "src=${parted_rule} dest=/root/disk-parted-rules-amd64.yml mode=0755"
		        if [ "$(ansible all --list-hosts |grep -c node)" -eq 1 ];then
                ansible nodes -m shell -a "bash -x part_disk.sh all --disk ${NODE_1_DEV}"
                tmux send -t install_offline "cd ${WORK_DIR}/awesome/viper/ && ansible-playbook infra.yml -e skip_compose_check=true --skip-tags disks| tee -a ${WORK_DIR}/install_infra.log" ENTER
            else
                ansible node-1 -m shell -a "bash -x part_disk.sh all --disk ${NODE_1_DEV}"
                ansible node-2 -m shell -a "bash -x part_disk.sh all --disk ${NODE_2_DEV}"
                ansible node-3 -m shell -a "bash -x part_disk.sh all --disk ${NODE_3_DEV}"
                tmux send -t install_offline "cd ${WORK_DIR}/awesome/viper/ && ansible-playbook infra.yml -e skip_compose_check=true --skip-tags disks| tee -a ${WORK_DIR}/install_infra.log" ENTER
            fi
	  else
	      echo "Disk order is correct."
		    tmux send -t install_offline "cd ${WORK_DIR}/awesome/viper/ && ansible-playbook infra.yml |tee -a ${WORK_DIR}/install_infra.log" ENTER
    fi

	  popd   || exit 1
}

###############################
###### sc部署步骤--infra  ######
###############################
function deploy_infra() {
    echo "###### install infra #####"
    pushd ${WORK_DIR}/awesome/viper   || exit 1
      edit_rc
      # 检查磁盘后直接执行对应ansible命令
      check_disk
      # 先预置成功标志，防止sep上任务直接失败，跑不到下一步
      echo 0 > /tmp/sc_deploy.log
      # 等待5s，等进程拉起
      sleep 5s
    popd   || exit 1
}

###########################################
###### sc部署步骤--检测infra是否部署成功  ######
###########################################
function check_infra() {
      # 检查相关进程，判断是否部署成功
      while true;do
         echo "#### check install infra ####"
         if [ "$(pgrep -cf 'infra.yml')" -eq 0 ]; then
             echo "#### install infra is end ...... ####"
             break
         else
             echo "#### install infra is ongoing ...... ####"
             sleep 30
         fi
      done

      # 校验infra部署是否成功--nodes是否ready
      ret=$(kubectl get nodes --no-headers|grep -vc Ready)

      if [ "$ret" -ne 0 ]; then
          echo "!!!ERROR: deploy_infra failed! check ${WORK_DIR}/install_infra.log now stop.."
          echo 1 > /tmp/sc_deploy.log
          exit 1
      fi

      # 取消服务器重启后自动部署配置
      sed -i /sensecenturio-deploy/d /etc/rc.d/rc.local
}

#################################
###### sc部署步骤--product  ######
#################################
function deploy_product() {
    echo "###### install component and engine apps #####"
    pushd ${WORK_DIR}/awesome/${PRODUCT_NAME}  || exit 1
      bash scripts/offline-deploy.sh install
      if [ "${ret}" -ne 0 ]; then
          echo "!!!ERROR: deploy_product failed! check /tmp/sc_deploy.log now stop.."
          echo 1 > /tmp/sc_deploy.log
          exit 1
      fi
    popd  || exit 1
}

###############################
###### 离线部署百夫长--全部 ######
###############################
function deploy_sc {
    if [ -n "${OPT_STEP}" ]; then
        case $OPT_STEP in
            infra)
                check_tmux
                deploy_infra
                ;;
            product)
                check_infra
                deploy_product
                ;;
            *)
                print_usage;
                exit 1;
                ;;
        esac
    else
        check_tmux
        deploy_infra
        check_infra
        deploy_product
    fi
}

function main() {
    while [[ $# -gt 0 ]]
    do
        key="$1"
        case $key in
            prepare)
                ACTION="prepare"
                shift
                ;;

            genconfig)
                ACTION="genconfig"
                shift
                ;;

            deploy)
                ACTION="deploy"
                shift
                ;;

            --package|-p)
                OPT_PACKAGE="$2"
                shift
                shift
                ;;

            --clean|-c)
                OPT_CLEAN="$2"
                shift
                shift
                ;;

            --step|-s)
                OPT_STEP="$2"
                shift
                shift
                ;;

            --node_1_dev)
                NODE_1_DEV="$2"
                shift
                shift
                ;;

            --node_2_dev)
                NODE_2_DEV="$2"
                shift
                shift
                ;;

            --node_3_dev)
                NODE_3_DEV="$2"
                shift
                shift
                ;;

            all)
                ACTION="all"
                shift
                ;;
            *)
                print_usage;
                exit 1;
        esac
    done

    if [ "${ACTION}" == "prepare" ]; then
        check_tmux
        get_pkgs_name
        prepare_packages
    elif [ "$ACTION" == "genconfig" ]; then
        generate_config
    elif [ "$ACTION" == "deploy" ]; then
        check_tmux
        deploy_sc
    elif [ "$ACTION" == "all" ]; then
        check_tmux
        get_pkgs_name
        prepare_packages
        generate_config
        deploy_sc
    else
        print_usage;
        exit 1
    fi
}

main "$@"
echo $? > /tmp/sc_deploy.log