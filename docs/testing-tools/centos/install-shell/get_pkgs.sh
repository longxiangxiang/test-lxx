#!/bin/bash

PRODUCT_NAME="sensecenturio"
PRODUCT_VERSION="6.2.x"
ARCH="amd64"
DAY=$(date +%Y%m%d)
ATTACHMENTS_PACKAGE="centurio-attachements-v6.2-amd64.tar.gz"

MAX_RETRIES=5
RETRY_INTERVAL=600 # 10 minutes in seconds
COOLDOWN_TIME=30

WORK_DIR="/data"
PKGS_HOST="http://10.9.242.24:10652"

function Usage() {
    echo "./get_pkgs.sh"
    echo "    --product_name: product name, default: sensecenturio"
    echo "    --product_version: product version, default: 6.2.x"
    echo "    --arch: device arch [amd64, arm64], default: amd64"
    echo "    --day: packags_build date [example: 20231116], default: today"
    echo "    --attachments_package: default: centurio-attachements-v6.2-amd64.tar.gz"
}




###################
# 检查包是否已经打好 #
###################
function check_pkgs(){
  for i in {1..5}; do
    FILES=$(curl -s "${PKGS_HOST}/tmp/Packages/${PRODUCT_NAME}/${PRODUCT_VERSION}/${ARCH}/${DAY}/" | grep -oP '(?<=href=")[^"]*' | grep ${PRODUCT_NAME})

    if [ -n "$FILES" ]; then
      echo "Found files: $FILES"
      break
    fi

    if [ "$i" -eq "$MAX_RETRIES" ]; then
      echo "Failed to fetch files after 5 attempts. Exiting."
      exit 1
    fi

    echo "Attempt $i failed. Retrying in $RETRY_INTERVAL seconds..."
    sleep "$RETRY_INTERVAL"
  done
}

###############################
# 使用tmux确保正常下载 #
###############################
function check_tmux() {
    # 确保tmux依赖有部署
    command -v tmux >/dev/null 2>&1 || { echo >&2 "tmux is not installed. Installing it now..."; yum install -y tmux; }

    if [ "$(tmux ls | grep -c download_pkgs)" -eq 0 ];then
        tmux new-session -d -s download_pkgs
    fi
}


###################
# 下载部署包 #
###################
function download_pkgs() {

    # 确保wget依赖有部署
    command -v wget >/dev/null 2>&1 || { echo >&2 "wget is not installed. Installing it now..."; yum install -y wget; }

    # 新建部署目录
    if [ ! -d ${WORK_DIR} ]; then
        mkdir ${WORK_DIR}
    fi

    # 下载部署包
    for file in ${FILES}; do
      if [ ! -f "${WORK_DIR}/${file}" ]; then
        echo "${file} not found in ${WORK_DIR}, downloading..."
        file_url="${PKGS_HOST}/tmp/Packages/${PRODUCT_NAME}/${PRODUCT_VERSION}/${ARCH}/${DAY}/${file}"
        tmux send -t download_pkgs "wget -O ${WORK_DIR}/${file} ${file_url}"  ENTER
        # 等待5s，等进程拉起
        sleep 5s
        # 检查进程
        check_download
        echo "Download ${file} complete!"
      else
        echo "${file} already exists in ${WORK_DIR}."
      fi
    done
}

function download_attachment() {
    # 下载依赖包
    if [ ! -f ${WORK_DIR}/${ATTACHMENTS_PACKAGE} ]; then
        echo "${ATTACHMENTS_PACKAGE} not found in ${WORK_DIR}, downloading..."
        file=${ATTACHMENTS_PACKAGE}
        file_url="http://sz.viper.sensesecurity.net/software/centurio-file/${ATTACHMENTS_PACKAGE}"
        tmux send -t download_pkgs "wget -O ${WORK_DIR}/${file} ${file_url}" ENTER
        # 等待5s，等进程拉起
        sleep 5s
        # 检查进程
        check_download
        echo "Download ${file} complete!"
    else
        echo "${file} already exists in ${WORK_DIR}."
    fi
}

##############################
# 等待tmux启动的下载部署包进程结束 #
##############################
function check_download() {
    while true;do
       echo "check download pkgs ${file}"
       if [ "$(pgrep -cf "${file_url}")" -eq 0 ]; then
           break
       else
           sleep ${COOLDOWN_TIME}
       fi
    done
}

function main() {
    while [[ $# -gt 0 ]]; do
       case "$1" in
          --product_name) # 产品名称,不填默认为：sensecenturio
              PRODUCT_NAME="$2"
              shift
              shift
              ;;
          --product_version) # 产品版本，不填默认为： 6.2.x
              PRODUCT_VERSION="$2"
              shift
              shift
              ;;
          --arch)  # cpu架构:arm64，amd64，不填默认为：amd64
              ARCH="$2"
              shift
              shift
              ;;
          --day)  # 部署包打包日期，不填默认为当天
              DAY="$2"
              shift
              shift
              ;;
          --attachments_package)  # 百夫长依赖包，不填默认为： centurio-attachements-v6.2-amd64.tar.gz
              ATTACHMENTS_PACKAGE="$2"
              shift
              shift
              ;;
          *)
              Usage;
              echo "1" > /tmp/get_pkgs.log  # sep平台成功/失败标志位判断文件
              exit 1;
        esac
    done

    if [[ ${PRODUCT_NAME} == "" || ${PRODUCT_VERSION} == "" || ${ARCH} == "" || ${ATTACHMENTS_PACKAGE} == ""  ]];then
        echo -e "\033[31m some param is null exit ... \033[0m"
        Usage;
        echo "1" > /tmp/get_pkgs.log
        exit 1;
    elif [[ ${DAY} == "" ]];then
        DAY=$(date +%Y%m%d)
    fi

    echo -e "\033[32m ***********************************************start get packages*********************************************** \033[0m"

    # 先检查包是否打好了
    check_pkgs;
    # 启动tmux终端
    check_tmux;
    # 下载部署包
    download_pkgs;
    download_attachment;
}

main "$@"
echo $? > /tmp/get_pkgs.log

