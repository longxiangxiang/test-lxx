# 服务器配置IP及BMC

[返回上一级](index.md)

## 修改服务器静态IP

1. 登录linux系统（默认账号密码一般在服务器上有）

2. 输入ip addr确认哪一个网口是开启的
![img001](../attachments/img001.png)

3.修改服务器IP设置
```shell
vim /etc/sysconfig/network-scripts/ifcfg-eth100

# 主要修改项为
IPADDR: 静态IP
GATEWAY: 网关
NETMASK: 子网掩码，一般改为255.255.255.0
DNS1: 域名服务器，一般改为10.8.8.8
ONBOOT: 是否激活网卡，一般改为yes
BOOTPROTO: 网络配置参数，一般改为static
```

4.重启网络服务
```shell
service network restart
```

5.校验

ping一下外网，能ping通的话说明配置没有问题，否则重新检查一遍配置

## 通过ipmitool配置bmc

1. 安装ipmitool
```shell
yum -y install ipmitool
```

2.常用命令
```shell
# 查看channel的信息，<channel>一般是0或者1，华三的是8
ipmitool lan print <channel> 

# 修改bmc的地址模式，<source> 为：static静态，none不生效，dhcp自动获取（举例ipmitool lan set 8 ipsrc static）
ipmitool lan set <channel> ipsrc <source>

# 修改bmc的ip
ipmitool lan set <channel> ipaddr 1.1.1.1

# 修改bmc的子网掩码
ipmitool lan set <channel> netmask 255.255.255.0

# 修改bmc的网关地址
ipmitool lan set <channel> defgw ipaddr 1.1.1.1

# 查看用户列表
ipmitool user list <channel>

# 修改用户的密码
ipmitool user set name <ID> <username>
ipmitool user set password <ID> < password >

# 查看用户详情
ipmitool channel getaccess <channel> <ID>

# 启用用户
ipmitool user enable <channel>

# 禁用用户
ipmitool user disable <channel>

# 修改用户权限（举例的是admin权限）
ipmitool channel setaccess <channel> <ID> callin=off ipmi=on link=on privilege=4
```

3.一般情况下操作步骤，举例说明，华三服务器
```shell
yum -y install ipmitool
ipmitool lan set 8 ipsrc static
ipmitool lan set 8 ipaddr 10.9.192.31
ipmitool lan set 8 netmask 255.255.255.0
ipmitool lan set 8 defgw ipaddr 10.9.192.1
ipmitool user set name 3 admin
ipmitool user set password 3 passwd-admin
ipmitool user enable 3
ipmitool channel setaccess 8 3 callin=off ipmi=on link=on privilege=4
```