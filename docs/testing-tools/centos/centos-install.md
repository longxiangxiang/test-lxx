# centos部署

[返回上一级](index.md)

## 装机

iso操作系统文件自己找

## 选择安装语种

选择 English 即可，选择后，点击右下角的 Continue 按钮 <br/>

![选择语种](../attachments/os_provisioner/image1.png)

## 选择系统时间

为便于后续定位问题和查看日志，建议配置好系统的本地时间 <br/>

‘DATE&TIME’时区选择中国-上海 ASIA <br/>


## 选择安装软件类型

”SOFTWARE SELECTION”选择standard <br/>

## 选择软件安装位置
1、如下图红框，选择“INSTALLATION DESTINATION”<br/>
![INSTALLATION DESTINATION](../attachments/os_provisioner/image5.png)

2、选择硬盘，且选择手动分盘，选好之后选择左上角的“Done”。<br/>
![手动分盘](../attachments/os_provisioner/image6.png)

3、进入自定义分区页面，系统盘分区规则如下<br/>

| 配置  | 参数           | 格式 |
| :---- | :------------- | :--- |
| /boot | 1GB            | xfs  |
| /nas  | 1GB            | xfs  |
| /     | 剩余所有容量   | xfs  |
| /swap | 不需要swap分区 | -    |

## 配置完成后，开始安装
![安装](../attachments/os_provisioner/image7.png)\

## 设置 root 用户密码

1、选择“ROOT PASSWORD”<br/>
![ROOT PASSWORD](../attachments/os_provisioner/image8.png)

2、密码请自行设置，完成后点击左上角的“Done”按钮<br/>
![密码](../attachments/os_provisioner/image9.png)

## 安装完成后，重启
![重启](../attachments/os_provisioner/image10.png)

安装完成后使用 `xfs_info / | grep ftype` 命令检查是否设置了 `ftype=1`
