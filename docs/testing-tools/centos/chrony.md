# chrony同步服务器时间

[返回上一级](index.md)

```shell
# 一般服务器默认有安装chrony，可以使用下面的命令查看
rpm -qa |grep chrony

# 如果没有安装环境可使用下面的命令安装（或者自己去下载离线包）
yum install chrony

# 查看chrony服务状态
systemctl status chronyd.service

# 若服务是关闭的，使用下面的命令打开
systemctl start chronyd.service　　# 启动chrony服务
systemctl enable chronyd.service　 # 设置开机同步时间

# 查看防火墙状态，若开启，可以设置允许ntp服务
systemctl status firewalld.service           # 查看防火墙状态
firewall-cmd --add-service=ntp --permanent   # 添加规则
firewall-cmd --reload                        # 重新加载防火墙配置，使得设置生效

# 修改配置文件；修改结果如下图
vi /etc/chrony.conf
## 添加：server 10.111.32.22 iburst

# 重启下服务端chrony服务，使修改的配置生效
systemctl restart chronyd.service

# 查看时间同步源：
chronyc sources -v

# 查看时间同步源状态
chronyc sourcestats -v

# 查看 Chrony 服务是否与 NTP 服务器同步
chronyc tracking
## 如果该命令返回结果为 Leap status : Normal，则代表同步过程正常。
```