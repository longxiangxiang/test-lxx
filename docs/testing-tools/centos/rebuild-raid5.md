# 重组raid5操作

[返回上一级](index.md)

## 1.安装`storcli`安装包
```
yum install -y storcli

ln -s /opt/MegaRAID/storcli/storcli64 /bin/storcli
```

## 2.查看磁盘组信息
```
eid=$(storcli /c0/eall/sall show|grep -o '[0-9].*:0'|awk -F ':' '{print $1}')

storcli /c0 show all
```

## 3.清除除包含sda的磁盘组并格式化磁盘：
```
# 格式化危险操作，确认数据不再使用或后面打算重装
for i in {b..k};do dd if=/dev/zero of=/dev/sd$i bs=1M count=100;done

for i in `seq 1 12` ;do storcli /c0/v$i del force;done

# 若新增加的磁盘带有原来的阵列信息，需要清楚后才能继续操作
storcli /c0/fall del

lsblk -a  # 查看有且只有sda信息
```

## 4.组建raid5等,适用单机部署（注意：组raid的数据盘必须大小一致）
```
storcli /c0 add vd r5 drives=252:2-6 WT NORA DIRECT

storcli /c0 add vd r5 drives=8:2-11 WT NORA DIRECT

组建raid0(可选)
storcli /c0 add vd r0 drives=252:7 WT NORA DIRECT

组建raid1(可选)单位MB
storcli /cx add vd r1 Size=983040 drives=252:6-7 wb ra

组建raid10(用于offline-business节点)
storcli /c0 add vd r10 drives=252:2-7  pdperarray=2
```

### 5. 单机节点还原集群各个节点(适用以前是标准集群拆出的单机)
```
eid=$(storcli /c0/eall/sall show|grep -o '[0-9].*:0'|awk -F ':' '{print $1}')

# 还原data-fusion节点
for i in `seq 2 11` ;do storcli /c0 add vd r0 drives=$eid:$i WT NORA DIRECT;done

# 还原engine-db节点
for i in `seq 2 5` ;do storcli /c0 add vd r0 drives=$eid:$i WT NORA DIRECT;done
storcli /c0 add vd r0 drive=$eid:6-9 WT NORA DIRECT
storcli /c0 add vd r1 drive=$eid:10-11 WT NORA DIRECT

# 还原controller节点
for i in `seq 2 7` ;do storcli /c0 add vd r0 drives=$eid:$i WT NORA DIRECT;done
```

### 6.重启服务器
```
\cp /etc/fstab /etc/fstab.bak

sed -i '/datafusion/d' /etc/fstab

sed -i '/structdb-elasticsearch/d' /etc/fstab

sed -i '/locals/d' /etc/fstab

sudo reboot
```

### 7.重启后查看磁盘是否对应
```
lsblk -a
```














