# 使用nfs服务器做nas

[返回上一级](index.md)

```shell
# 找空闲的服务器，然后关闭防火墙
sudo systemctl stop firewalld

# 查找空闲的数据盘
lsblk

# 格式化空闲的数据盘（以sdc为例）
mkfs.xfs /dev/sdc -f

# 安装nfs包及依赖
yum install -y rpcbind nfs-utils

# 建nfs_path目录
mkdir /nfs_path

# 修改配置文件
lsblk -f         # 查询磁盘UUID
vi /etc/fstab    # 新增：UUID=fe241c56-13b3-451a-92e5-db53eb485b99 /nfs_path      xfs     defaults,noatime        0       0
vi /etc/exports  # /nfs_path/nas *(rw)

# 启动nfs服务
systemctl start rpcbind nfs
systemctl enable rpcbind nfs

# 查询nfs服务器上共享目录信息
showmount -e 10.9.244.99    # 10.9.244.99为nfs服务器

# 挂载nfs_path
mount -a

# 建nas目录
mkdir -p /nfs_path/nas

# 给与权限
chmod -R 777 /nfs_path/

# 这样我们就可以得到一个可以做nas的nfs共享目录
10.9.244.99:/nfs_path/nas
```