# EXPLAIN

EXPLAIN 是 MySQL 中用于查看查询执行计划的关键字。通过 EXPLAIN 命令，你可以获取关于查询的信息，包括表的读取顺序、连接类型、索引使用情况等

基本语法是

```sql
EXPLAIN SELECT * FROM your_table WHERE your_condition;

-- 返回的结果有：
-- id： 查询的标识符，对于多表查询，每个表都有一个唯一的标识符。
-- select_type： 查询的类型，例如 SIMPLE、PRIMARY、SUBQUERY 等。
-- table： 查询涉及的表。
-- type： 表的连接类型，例如 ALL（全表扫描）、INDEX（索引扫描）、RANGE（范围扫描）等。
-- possible_keys： 查询可能使用的索引。
-- key： 实际使用的索引。
-- key_len： 使用的索引长度。
-- ref： 显示索引如何被使用，例如 const（常量条件）、NULL（未使用索引）等。
-- rows： 估计的行数。
-- Extra： 其他信息，例如 Using where、Using filesort 等。
```