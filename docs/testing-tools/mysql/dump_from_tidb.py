import pandas as pd
from sqlalchemy import create_engine
import requests

# TiDB数据库连接信息
tidb_host = '10.111.32.69'
tidb_port = '30200'
tidb_database = 'senseface'

# 获取 TiDB 密码
password_api_url = 'http://172.20.25.158:8888/get_sc_info2'
password_payload = {"page": 1, "sc_host": tidb_host}
response = requests.post(password_api_url, json=password_payload)
data = response.json().get("data", [])[0]

if not data:
    raise ValueError("Failed to retrieve TiDB credentials from the API.")

tidb_username = data["tidb_root_user"]
tidb_password = data["tidb_root_pwd"]


# 构建TiDB连接字符串
tidb_connection_str = f'tidb://{tidb_username}:{tidb_password}@{tidb_host}:{tidb_port}/{tidb_database}'

# 创建TiDB引擎
engine = create_engine(tidb_connection_str)

# 执行SQL查询
sql_query = "SELECT c.camera AS camera_serial, s.zone_serial, c.region_id, c.camera_id FROM senseface.sync_camera_viper c, senseface.info_video_resource s WHERE c.camera=s.serial;"
df = pd.read_sql_query(sql_query, engine)

# 将结果导出为CSV文件
csv_file_path = f'output-{tidb_host}.csv'
df.to_csv(csv_file_path, index=False)

print(f"Data exported to {csv_file_path}")
