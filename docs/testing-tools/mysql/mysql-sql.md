# mysql 常用基础命令集

## 登录 MySQL 服务器
```sql
mysql -u username -p
```

## 显示数据库列表：
```sql
SHOW DATABASES;
```

## 显示所有表
```sql
SHOW TABLES;
```

## 显示表结构
```sql
DESCRIBE table_name;
```

## 创建数据库
```sql
CREATE DATABASE database_name;
```

## 创建表
```sql
CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    ...
);
```

## 插入数据
```sql
INSERT INTO table_name (column1, column2, ...) VALUES (value1, value2, ...);
```

## 查询数据
```sql
SELECT (column1, column2, ...) FROM  table_name  WHERE condition;
```

## 更新数据
```sql
UPDATE table_name SET column1 = value1, column2 = value2, ... WHERE condition;
```

## 删除数据
```sql
DELETE FROM table_name WHERE condition;

TRUNCATE table_name;
```

## 删除表
```sql
DROP TABLE table_name;
```

## 删除数据库
```sql
DROP DATABASE database_name;
```

## 内连接（INNER JOIN）
* 返回两个表中符合连接条件的行。
* 结果集中只包含两个表中列值匹配的行。
```sql
SELECT *
FROM table1
INNER JOIN table2 ON table1.column = table2.column;
```

## 左连接（LEFT JOIN 或 LEFT OUTER JOIN）
* 返回左表中所有行和右表中符合连接条件的行。
* 如果右表中没有匹配的行，会在结果集中显示 NULL 值。
```sql
SELECT *
FROM table1
LEFT JOIN table2 ON table1.column = table2.column;
```

## 右连接（RIGHT JOIN 或 RIGHT OUTER JOIN）
* 返回右表中所有行和左表中符合连接条件的行。
* 如果左表中没有匹配的行，会在结果集中显示 NULL 值。
```sql
SELECT *
FROM table1
RIGHT JOIN table2 ON table1.column = table2.column;
```

## 全外连接
* 返回左右两表中所有的行，无论是否符合连接条件。
* 如果某一边没有匹配的行，会在结果集中显示 NULL 值。
```sql
SELECT *
FROM table1
FULL JOIN table2 ON table1.column = table2.column;
```

## 自连接
* 表示连接表与自身，常用于树状结构或层次结构的查询。
* 通过使用别名来区分连接到自身的两个实例。
```sql
SELECT e1.employee_id, e1.employee_name, e2.manager_name
FROM employees e1
JOIN employees e2 ON e1.manager_id = e2.employee_id;
```

## 使用 WHERE 子句进行条件联接
* 在联接时通过 WHERE 子句添加额外的条件，限制结果集
```sql
SELECT *
FROM orders
JOIN customers ON orders.customer_id = customers.customer_id
WHERE customers.country = 'USA';
```

## 多表联接
* 联接多个表，通过连接条件将多个表关联在一起
```sql
SELECT *
FROM orders
JOIN customers ON orders.customer_id = customers.customer_id
JOIN products ON orders.product_id = products.product_id;
```

## 使用别名进行联接
* 为表起别名，提高 SQL 查询的可读性。
* 在查询中使用别名来引用表，例如避免歧义。
```sql
SELECT o.order_id, c.customer_name
FROM orders AS o
JOIN customers AS c ON o.customer_id = c.customer_id;
```