# docker 安装

[返回上一级](index.md)

## 安装必要的软件包
```shell
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

## 添加 Docker 的官方仓库
```shell
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

## 安装 Docker CE
```shell
sudo yum install docker-ce
```

## 启动 Docker 服务
```shell
sudo systemctl start docker
```

## 设置 Docker 开机自启动
```shell
sudo systemctl enable docker
```

## 验证 Docker 是否成功安装
```shell
sudo docker --version
```
