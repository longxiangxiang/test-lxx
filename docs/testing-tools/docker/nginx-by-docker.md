# 通过docker起nginx

[返回上一级](../index.md)

> 先确保环境已经部署好docker服务

## 先安装docker-compose
```shell
# 安装docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# 查看docker-compose版本
docker-compose --version
```

## 准备docker-compose.yml文件
```yaml
version: '2'
services:
  nginx:
    image: nginx:latest  # nginx镜像
    restart: always
    container_name: nginx-fs # 容器名称
    ports:
      - "8828:443" # 端口映射关系，左边是宿主机端口，右边是容器的端口
    volumes: # 目录/文件映射关系，左边是宿主机的目录/文件，右边是容器的目录/文件
      - /var/run/docker.sock:/var/run/docker.sock # docker.sock映射
      - /data/picture:/home/nginx/picture  # 其他挂载目录映射
      - ./default.conf:/etc/nginx/conf.d/default.conf # default.conf映射
      - /data/ssl/server.crt:/server.crt # 证书文件映射
      - /data/ssl/server.key:/server.key # 证书文件映射
```

<details>
    <summary>举例</summary>

    ```yaml
    version: '2'
    services:
      nginx:
        image: nginx:latest
        restart: always
        container_name: nginx-fs
        ports:
          - "8828:443"
          - "8837:80"
        volumes:
          - /var/run/docker.sock:/var/run/docker.sock
          - /data/picture:/home/nginx/picture
          - ./default.conf:/etc/nginx/conf.d/default.conf
          - /data/ssl/server.crt:/server.crt
          - /data/ssl/server.key:/server.key
    ```
</details>

## 准备default.conf文件
> 即nginx.conf文件,以下举例作为文件服务器的配置

<details>
    <summary>举例</summary>

    ```yaml
    # http配置
    server {
        listen       80;
        server_name  localhost;
        charset utf-8;
    
        location ~ {
            #在docker内nginx的目录
            root /home/nginx;
            expires 1d;
            allow all;
            autoindex on;
            autoindex_localtime on;
            autoindex_exact_size off;
        }
    }
    
    # https配置
    server {
        listen       443 ssl;
        server_name  localhost;
        charset utf-8;
    
        ssl_certificate     /server.crt;
        ssl_certificate_key /server.key;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    
        location ~ {
            #在docker内nginx的目录
            root /home/nginx;
            expires 1d;
            allow all;
            autoindex on;
            autoindex_localtime on;
            autoindex_exact_size off;
        }
    }
    ```
</details>

## 准备证书文件
```text
openssl genrsa -out server.key 2048
openssl req -new -x509 -sha256  -key server.key -out server.crt
```

## 准备nginx镜像
```shell
# 在线环境可以直接拉在线的镜像
docker pull nginx
```

## 启动服务
> 在docker-compose.yml文件的同级目录下执行
```shell
docker-compose up -d
```