#!/usr/bin/bash

# 获取 Kafka 用户名和密码
kafka_user_name=$(kubectl get secrets password-secrets -oyaml | grep kafka_admin_username | awk '{print $2}' | base64 -d)
kafka_password=$(kubectl get secrets password-secrets -oyaml | grep kafka_admin_password | awk '{print $2}' | base64 -d)

# Kafka 配置文件
kafka_config="/opt/kafka_2.12-2.2.2/bin/kafka_client_jaas.conf"

# Kafka 服务器
kafka_server="kafka-default.component.svc.cluster.local:9092"

# 获取所有业务层 topic 列表
kafka_topics=$(kubectl exec -it kafka-default-0 -n component -- /opt/kafka_2.12-2.2.2/bin/kafka-topics.sh --bootstrap-server $kafka_server --list --command-config $kafka_config)

# 根据参数设置要消费的 topic
case $1 in
    face)
        topic="WhaleCollect"
        ;;
    body)
        topic="WhaleBodyCollect"
        ;;
    crowd)
        topic="WhaleCrowdCollect"
        ;;
    event)
        topic="WhaleAlgoCollect"
        ;;
    push)
        topic="GeneralPush"
        ;;
    alarm)
        topic="SensefaceDetect"
        ;;
    capture)
        topic="SensefacePush"
        ;;
    garbage)
        topic="WhaleGarbageCollect"
        ;;
    list)
        echo "业务层 Topic 列表: $kafka_topics"
        exit
        ;;
    group)
        kubectl exec -it -n component kafka-default-0 -- /opt/kafka_2.12-2.2.2/bin/kafka-consumer-groups.sh --bootstrap-server $kafka_server --list --command-config $kafka_config
        exit
        ;;
    listen)
        if [ -z $2 ]; then
            echo "请提供要监听的 Topic"
            exit 1
        else
            topic=$2
        fi
        ;;
    *)
        echo "用法: $0 [face|body|crowd|event|push|alarm|capture|garbage|list|group|listen]"
        exit 1
        ;;
esac

# 生成 Kafka 客户端配置文件
cat > $kafka_config <<EOF
KafkaClient {
  org.apache.kafka.common.security.plain.PlainLoginModule required
  username="$kafka_user_name"
  password="$kafka_password";
};
EOF

# 执行 Kafka 消费者
kubectl exec -it -n component kafka-default-0 -- /opt/kafka_2.12-2.2.2/bin/kafka-console-consumer.sh --bootstrap-server $kafka_server --topic $topic --consumer-property security.protocol=SASL_PLAINTEXT --consumer-property sasl.mechanism=PLAIN --consumer.config $kafka_config
