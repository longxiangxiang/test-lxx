# redis操作技巧

[返回上一级](index.md)

## redis指令

### 进入redis命令行
```shell
REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h 127.0.0.1 -p 6379 -c
```

### 常用redis操作指令
```shell
# 查询指定的键
KEYS [key]

# 获取键的值
## 字符串操作
GET [key]
## 哈希操作
HGET [key] [field]
HGETALL [key]

# 设置键值
## 字符串操作
SET [key] [value]
## 哈希操作
HGETALL [key] [field]

# 删除指定的键
DEL [key]

# 举例：批量删除点位缓存
kubectl exec -it -n component redis-default-master-0 -- bash -c 'REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h 10.111.32.71 -p 10200 -c -c keys Whale:Camera:*' | awk -F ""\" '{print $2}'| xargs -I {} kubectl exec -it -n component redis-default-master-0 -- bash -c 'REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h 10.111.32.71 -p 10200 -c -c del "$1"' -- {}
```