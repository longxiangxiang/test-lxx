# K8S端口暴露

[返回上一级](index.md)

出于安全要求，默认情况下大多数服务的端口号都是被屏蔽的，但是实际工作中会需要进行端口访问，需要打开相关端口

> 1.查询目标服务端口号映射
```shell
kubectl get services -o wide -n [namespace]

或
 
kubectl -n [namespace] get svc
```

> 2.生成 expose-service.yml （名字不要紧）文件，内容如下（根据实际需要添加或修改）：

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: mysql-ext
  namespace: component
spec:
  ports:
  - name: mysql-ext
    nodePort: 10208
    port: 3306
    protocol: TCP
    targetPort: 3306
  selector:
    component: mysql-default
  sessionAffinity: None
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: es-ext
  namespace: component
spec:
  ports:
  - name: es-http-ext
    nodePort: 10228
    port: 9200
    protocol: TCP
    targetPort: 9200
  selector:
    component: elasticsearch
    role: es-data-default
  sessionAffinity: None
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: es-tcp-ext
  namespace: component
spec:
  ports:
  - name: mysql-ext
    nodePort: 10229
    port: 9300
    protocol: TCP
    targetPort: 9300
  selector:
    component: elasticsearch
    role: es-data-default
  sessionAffinity: None
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-video-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10601
    port: 10601
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-video
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-portrait-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10603
    port: 10603
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-portrait
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-sync-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10213
    port: 10213
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-sync
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-openapi-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10212
    port: 10212
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-openapi
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-jail-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10625
    port: 10625
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-jail
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-scheduled-ext
  namespace: whale-component
spec:
  selector:
    app: whale-scheduled
  ports:
    - name: http
      protocol: TCP
      nodePort: 10214
      port: 8080
      targetPort: 8080
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-authorization-ext
  namespace: whale-component
spec:
  ports:
    - nodePort: 10612
      port: 10612
      protocol: TCP
      targetPort: 8080
  selector:
    app: whale-authorization
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-device-manager-ext
  namespace: whale-component
spec:
  ports:
    - nodePort: 10630
      port: 10630
      protocol: TCP
      targetPort: 8080
  selector:
    app: whale-device-manager
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-push-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10221
    port: 10221
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-push
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-task-center-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10620
    port: 10620
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-task-center
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-general-service-ext
  namespace: whale-component
spec:
  selector:
    app: whale-general-service
  ports:
    - name: http
      protocol: TCP
      nodePort: 10615
      port: 10615
      targetPort: 8080
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: whale-setting-ext
  namespace: whale-component
spec:
  ports:
  - nodePort: 10602
    port: 10602
    protocol: TCP
    targetPort: 8080
  selector:
    app: whale-setting
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: flink-ui
  namespace: whalebase
spec:
  ports:
  - nodePort: 18081
    port: 18081
    protocol: TCP
    targetPort: 18081
  selector:
    app: flink-job
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  name: nacos-ext
  namespace: whalebase
spec:
  ports:
  - nodePort: 18848
    port: 8848
    protocol: TCP
    targetPort: 8848
  selector:
    app: nacos
  type: NodePort
```

> 3.应用配置生效
```shell
kubectl apply -f expose-service.yml
```