# K8S相关操作

[返回上一级](index.md)

## 查看pod
```shell
kubectl get pod ( -n default )   # viper的engine服务在此namespace下
kubectl get pod -n whale-component   # 业务的应用服务在此namespace下
kubectl get pod -n whalebase   # 业务的基础服务在此namespace下
kubectl get pod -n component   # viper的基础服务在此namespace下
kubectl get pod -n batch   # viper的聚类、技战法服务在此namespace下
kubectl get pod -n {namespace} -o wide   # -owide参数可以显示pod所在节点和对应ip 
kubectl get pod --all-namespaces   # 查看所有namespace下的pod
```

## 重启pod
```shell
kubectl delete pod ( -n {namespace} ) {pod-name}
# 示例
kubectl delete pod -n whale-component whale-video-xxxxx-xxxxx
```

## 停止服务
```shell
kubectl delete -f 配置文件or配置文件目录
```

## 启动服务
```shell
kubectl apply -f 配置文件or配置文件目录
```

## 强制删除pod（慎用）
```shell
kubectl delete pod {pod-name} ( -n {namespace} ) --grace-period=0 --force 
```

## 重新加载配置文件
```shell
kubectl replace -f 配置文件or配置文件目录
```

## 查看pod的信息
```shell
kubectl describe pod ( -n {namespace} ) {pod-name}
# 示例
kubectl describe pod -n whale-component whale-video-xxxxx-xxxxx
```

## 查看pod日志
```shell
kubectl logs -f ( -n {namespace} ) {pod-name} ( --since 10m )
# --since 10m是只打印10分钟内的日志，日志太多的时候可以使用这个参数
# 示例
kubectl describe pod -n whale-component whale-video-xxxxx-xxxxx
```

## 查看pod配置
```shell
kubectl get pod ( -n {namespace} ) {pod-name} -o yaml
# 示例
kubectl get pod -n whale-component whale-video-xxxxx-xxxxx -o yaml
```

## 查看node
```shell
kubectl get node
kubectl get node -o wide   # -owide参数可以显示node对应ip 
```

## 查看service
```shell
kubectl get svc ( -n {namespace} )
# TYPE为ClusterIP是不对外暴露的服务，TYPE为NodePort是对外暴露的服务，PORT(S)可以看到接口的映射情况
```

## 查看pv
```shell
kubectl get pv ( -n {namespace} )
```

## 查看pvc
```shell
kubectl get pvc ( -n {namespace} )
```