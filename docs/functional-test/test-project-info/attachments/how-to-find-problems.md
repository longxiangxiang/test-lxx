# 百夫长排查问题思路

[返回上一级](../index.md)

**前言：**

本文默认读者已经掌握linux和浏览器开发者工具的基本操作

以下讲解的步骤不都是必要的排查步骤，当你操作熟练之后，可以根据自己的判断跳过一些步骤，可以更快地定位到问题。



## 一、确认报错的组件

### 方法一：

1. 首先打开`F12-network`

2. 再进行一次产生报错的操作

3. 找到报错的接口，查看接口的地址，通常为`https://sc_ip:10220/whale-openapi/{path1}/{path2}`

其中path1可以判断它对应的组件

### 方法二：

1. 打开`whale-openapi`的日志

2. 再进行一次产生报错的操作

3. 常见会有两种情况的报错：

* 报错一：
```
WARN c.s.i.w.c.rest.BaseResultRestClient - ~~ whale request failed, url=(POST)http://whale-xxxxx/......
```

报错是请求某个组件的接口报错，然后去查看`whale-xxxxx`组件的日志即可

* 报错二：未打印组件的接口

说明未请求组件之前，whale-openapi已经报错了，需要具体情况具体分析

4. 如果没有日志打印，说明请求未到达`whale-openapi`，可以查看`whale-gateway`以及`nginx-0`日志



## 二、查看对应组件的日志

1. 首先打开对应组件的日志：
* 方法一：直接用`k8s`命令查看组件日志
```
# 增量查看video服务10min内日志
kubectl logs -f -n whale-component whale-video-XXXXXX --since 10m  
```
* 方法二：到对应的pod日志目录查看
```
# 先确认该pod是起在哪个节点(单机环境不用想就在node-1)，以video服务为例：
kubectl get pod -n whale-component -owide|grep video

# 去对应日志目录查看日志：
百夫长5.0及以前：/admin/storage/0/whalelog/
百夫长5.1及5.2： /data0/logs/
百夫长6.0：/mnt/locals/logs/components
```

2. 再进行一次产生报错的操作，然后查看对应日志



## 三、如果对应组件未打印日志？

往上一级排查

数据流：nginx→gateway→openapi→组件



## 四、查看日志的注意事项

1. 不要看error日志，不要看error日志，不要看error日志！

error日志没有前后文，不好确认问题，请查看完整的日志

2. 不要看下面这类信息，这种信息开发才看的懂，主要关注这类信息前后的日志，那些日志才是关键

        at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:88)
        at com.sensetime.iva.whale.sync.core.aspects.viper.ViperAspectCommonService.single(ViperAspectCommonService.java:170)
        at com.sensetime.iva.whale.sync.core.aspects.viper.ViperAspectCommonService.single(ViperAspectCommonService.java:148)
        at com.sensetime.iva.whale.sync.invoker.aspect.ViperAspect.around(ViperAspect.java:43)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)
        at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)
        at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)
        at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)
        at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)
        at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:95)
        at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)
        at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)
        at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:691)
        at com.sensetime.iva.whale.sync.aggregate.service.impl.viper.ViperCMSCameraServiceImpl.access(ViperCMSCameraServiceImpl.java:79)
        at com.sensetime.iva.whale.sync.aggregate.service.impl.viper.ViperCameraServiceImpl.access(ViperCameraServiceImpl.java:86)
        at com.sensetime.iva.whale.video.core.APIClient.WhaleSyncServiceImpl.access(WhaleSyncServiceImpl.java:81)
        at com.sensetime.iva.whale.video.core.aspect.VideoResourceServiceCMSAspect.resourceAccess(VideoResourceServiceCMSAspect.java:121)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)

3. 与viper相关的一些操作，比如接入视频源，优先找到请求viper的接口打印日志
