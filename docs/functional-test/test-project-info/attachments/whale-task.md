# 布控

[返回上一级](../index.md)

## 布控任务漏告警或无告警，如何排查？
### 1、告警产生的链路
`viper kafka` <br/>
---> <br/>
`whale-collective`<br/> 
---> <br/>
业务层`kafka`，对应`topic`：`WhaleCollect`（人脸）`WhaleBodyCollect`(人体) `WhaleAlgoCollect`（算法仓）等<br/>
---> <br/>
`whale-realtime-detect`<br/>
---> <br/>
告警数据写入`ES`<br/>
---> <br/>
`whale-push`

### 2、首先查看`viper`的`kafka`有没有数据
#### 2.1、登录到`viper`平台服务器，进入`kafka`容器

```
kubectl exec -it -n component kafka-default-0 bash
```

#### 2.2、消费对应`topic`下的数据
```
cd /opt/kafka_2.12-2.2.2/bin/

./kafka-console-consumer.sh --bootstrap-server ${viper_ip}:9092 --consumer.config /opt/kafka/config/sasl.properties --topic stream.features.face_24902   # ${viper_ip}替换成viper平台管控节点ip

# 如果viper的kafka没有数据，排查下vps，ips等
```

### 3、然后查看`whale-collective`服务是否正常
#### 3.1、检查厂家设置-底层服务支持中`viper`平台`kafka`相关配置是否正确

#### 3.2、检查`whale-collective`是否正确建立监听
* whale-collective监听的topic来自viper接口：
```
GET  https://${viper_ip}:30443/engine/process-http-callback/v1/get_system_info         # ${viper_ip}替换成viper平台管控节点ip
```

* 若该接口无返回，则建立不了对应的监听，可以登陆viper服务器上手动启动服务：
```
kubectl apply -f /etc/kubernetes/apps/engine-process-callback-service/default/
```

* 若该接口返回的topic有误，可以修改viper服务器上配置文件：
```
vi /etc/kubernetes/apps/engine-process-callback-service/default/config.yml
```

* 然后重启viper服务器上服务
```
kubectl delete -f /etc/kubernetes/apps/engine-process-callback-service/default/

kubectl apply -f /etc/kubernetes/apps/engine-process-callback-service/default/
```
* 百夫长4.3版本开始，如果这个接口异常，也可以登陆厂家设置，底层服务支持，直接添加需要监听的`kafka`的`topic`，如果发现监听的`topic`为空，可以尝试重新保存一次配置，这样后台会重新获取一次`topic`列表

#### 3.3、查看`whale-collective`是否有消费到`viper`平台`kafka`数据
* 进入百夫长kafka容器
```
百夫长5.0及以前版本： 
kubectl exec -it -n whalebase kafka-0 bash

百夫长5.1及以后版本： 
kubectl exec -it kafka-default-0 -n component bash
```

* 新建配置文件
```
百夫长5.0及以前版本： cd /opt/kafka_2.12-1.0.0/bin/
百夫长5.1及以后版本： cd /opt/kafka_2.12-2.2.2/bin/

vi kafka_client_jaas.conf

# 文件内容如下,${viper_kafka_user_name}以及${viper_kafka_password}需手动替换成viper平台kafka用户名以及密码：

KafkaClient {
  org.apache.kafka.common.security.plain.PlainLoginModule required
  username="${viper_kafka_user_name}"
  password="${viper_kafka_password}";
};
```

* 消费viper平台kafka对应topic数据，${viper_ip}以及$topic替换成viper平台管控节点ip以及要消费的平台topic名称
```
export KAFKA_OPTS=" -Djava.security.auth.login.config=kafka_client_jaas.conf"
./kafka-console-consumer.sh --bootstrap-server ${viper_ip}:9092 --topic $topic --consumer-property security.protocol=SASL_PLAINTEXT --consumer-property sasl.mechanism=PLAIN
```

* 如果在百夫长服务器消费不到viper的kafka，可以排查下是网络还是其他什么原因


#### 3.4、查看`whale-collective`日志，看下数据处理是否正常

**需要排查`whale-collective`时，可以开启`debug`模式，将`nacos`中配置文件`whale-collective.yaml`的`debugMode`置为`true`，重启`whale-collective`，这样能看到接收的完整数据，注意检查完要改`false`，否则日志会暴涨**

* 检查日志是否出现`SKIP because camera serial is empty`
```
# 查看mysql中sync_camera_viper表是否有对应关系
SELECT * FROM sync_camera_viper where server = '${sync_server_config表的serial字段}' and region_id = ${regionId} and camera_id = ${cameraId}

# 如果没有查到对应关系，说明这路视频在业务层不存在，可以在业务层补充对应camera_id和region_id的视频源，或者把viper中这一路视频源任务删掉
```

* 检查日志是否出现`Camera info not matched in redis cache. camera serial:`或`Skip because camera is not in redis cache. redis key=`
```
# 检查redis缓存中，Whale:Camera:${cameraSerial}是否存在，如果不存在，尝试重启whale-video服务
```

* 检查日志是否出现`eventSend kafka error:`
```
# 检查业务kafka是否异常
```

* 检查日志是否出现`Unknown type_portrait url(imageId). Skipping`
```
# 该报错一般为消费到的数据json结构不对，请检查数据结构
```

### 4、查看百夫长的kafka有没有数据

* 进入百夫长kafka容器
```
百夫长5.0及以前版本： 
kubectl exec -it -n whalebase kafka-0 bash

百夫长5.1及以后版本： 
kubectl exec -it kafka-default-0 -n component bash
```

* 消费对应topic下的数据
```
# 百夫长5.0及以前版本：
cd /opt/kafka_2.12-1.0.0/bin/
./kafka-console-consumer.sh --bootstrap-server 127.0.0.1:10209 --topic WhaleCollect

# 百夫长5.1及以后版本：
cd /opt/kafka_2.12-2.2.2/bin/
./kafka-console-consumer.sh --bootstrap-server kafka-default.component.svc.cluster.local:9092 --topic WhaleCollect --consumer.config /opt/kafka/config/sasl.properties 
```

* 如果业务的kafka没有数据，可以排查下whale-collective是否异常

### 5、查看`whale-realtime-detect`是否正常
* 网页登录http://${百夫长IP}:18081
查看是否有Running Job，查看job里的Bytes received，Record received等属性<br/>
如果没有Running Job，可以手动启动
```
# 百夫长5.0及以前版本：
cd /admin/businessApp/SenseCity/services/whale-realtime-detect
./whale-realtime-detect-k8s.sh

# 百夫长5.1及以后版本： 
cd  /etc/kubernetes/apps/business-streamingjob/centurio-streamingjob
kubectl apply -f job.yml

```

* 查看flink日志是否有报错
```
# 百夫长5.0及以前版本：
/admin/storage/0/container/log/flink/job/console/
 
/admin/storage/0/container/log/flink/task/console/

# 百夫长5.1及以后版本： 
/mnt/locals/logs/flink-job-business/volume0

/mnt/locals/logs/flink-task-business/volume0
```

* 日志正常，但是无告警产生？
```
# 开启1：N的debug日志，方法是：

nacos中whale-sync.yaml的silenceMethods字段下StaticFeatureDBProxyServiceImpl.featureBatchSearchMulti删掉,重启whale-sync（ps：排查完问题要重新加回去，不然1：N的日志会非常多！）

sync日志目录：
# 百夫长5.0及以前版本：
/admin/storage/0/whalelog/whale-sync/

# 百夫长5.1及以后版本： 
/data0/logs/whale-sync/

# 或者使用k8s命令查看
kubectl logs -f -n whale-component whale-sync-xxx --since 5m

# 查看日志中POST /engine/alert-feature/v1/batch_search_multi接口的返回，具体再排查原因，常见的返回为空，则是没有比中，可以根据比分尝试把布控阈值调低再看看

# 若没有查看到调用1：N的日志，则检查下coredns是否有问题，若coredns有问题，需要重启coredns；然后重启业务服务和flink-job
```

### 6、查看es有没有数据 
浏览器打开百夫长ip:10300；然后输入http://百夫长ip:10228，然后点击连接（记得http一定不能省略，不然会连接不上）

选择数据浏览，点击alarm_index（布控告警）或者algo_index（算法仓告警）或者crowd_index（人群告警）查看

* 如果没有数据，可以排查下realtime是否异常，抓拍是否有比中，es的写入是否正常等
* 常见问题：磁盘空间快满了，触发es保护机制，转换为只读模式

解决办法：清理磁盘空间后，解除自读模式即可

```
curl -XPUT -H "Content-Type: application/json" http://百夫长ip:10228/_all/_settings -d "{\"index.blocks.read_only_allow_delete\": null}"
```

### 7、查看推送是否正常
如果es有告警数据，页面上可以查询到告警记录，但是无告警推送，实时监控无抓拍推送<br/>
登陆业务服务器，查看mpush和push日志<br/>
```
push日志 ：
# 百夫长5.0及以前版本：
/admin/storage/0/whalelog/whale-push/

# 百夫长5.1及以后版本： 
/data0/logs/whale-push/

# 或者使用k8s命令查看
kubectl logs -f -n whale-component whale-push-xxx --since 5m

mpush日志：
kubectl logs -f -n whalebase mpush-0
```
* 常见问题，push连接mpush失败，可以尝试重启mpush，等mpush启动完成后，重启push
```
kubectl delete pod -n whalebase mpush-0

# 等mpush重启完成后，重启whale-push 
kubectl delete pod -n whale-component whale-push-xxx
```

### 8、查看app是否正常
* 查看app是否已启动
登陆viper的console，https://viper平台管控节点ip:30900

系统服务->场景算法仓->算法仓管理

以上图为例，算法版本是20000，状态是激活，如果状态是未激活，说明该算法未授权，需要先申请授权。

如果console挂了，可以通过接口查看app列表（列举算法应用信息），下文同此，如果console正常可以不用接口再操作一遍：
```
GET {{host}}/engine/algo-store/v1/apps?page.offset=0&page.limit=100

# page.limit  一页显示多少个app
# page.offset  偏移量
# 举例：查询0-100 app page.offset=0&page.limit=100
        查询100-200 app page.offset=100&page.limit=100
        查询60-100 app page.offset=60&page.limit=40
```

version：算法版本<br/>
status：状态，ACTIVATED是激活<br/>

系统服务->场景算法仓->APPLET运行监控<br/>
已启动的算法会在这里列出来，如果没有，说明算法未启动，需要先启动<br/>
系统服务->场景算法仓->算法仓管理->算法详情（点击算法卡片）→启动（显卡根据实际情况选择，配置项一般按照默认即可）<br/>
查询接口（列举算法应用实例）：<br/>
```
GET {{host}}/engine/algo-store/v1/instances?page.offset=0&page.limit=100


# page.limit  一页显示多少个app
# page.offset  偏移量
# 举例：查询0-100 app page.offset=0&page.limit=100
        查询100-200 app page.offset=100&page.limit=100
        查询60-100 app page.offset=60&page.limit=40
 
# 其他可选过滤参数
# filters.algo_types 算法类型过滤，ALGO_VIDEO_PROCESS：视频类，以后应该会有图片类的
GET {{host}}/engine/algo-store/v1/instances?page.offset=0&page.limit=100&filters.algo_types=ALGO_VIDEO_PROCESS
```
启动app接口（创建算法应用实例）:<br/>
```
POST {{host}}/engine/algo-store/v1/apps/app_id/instances

# app_id 可以通过上文查询app列表的接口查看
{
    "app_id": "60",
    "user_configs": {
        "by_hardware": {
            "nv_p4": {
                "config_items": [],
                "replicas": {
                    "lower_bound": 1,
                    "upper_bound": 2
                }
            },
            "nv_t4": {
                "config_items": [],
                "replicas": {
                    "lower_bound": 1,
                    "upper_bound": 2
                }
            }
        }
    }
}
```

* 查看模型版本是否对齐
上文已经说明如何查看算法模型版本，还需要注意的是，有可能有多个版本的算法存在，要确认启动的算法版本跟你使用的一致<br/>

* 百夫长配置模型版本的方法：
登录到厂家设置-->其他设置-->事件配置设置&算法仓版本配置<br/>


* 查看任务的状态
登陆viper的console，https://viper平台管控节点ip:30900<br/>
系统服务->场景算法仓→APPLET解析管理<br/>
如果任务状态是运行中说明正常，否则需要到viper排查原因。<br/>
任务查询接口：<br/>
```
GET {{host}}/engine/camera-manager/v1/zones/{{zone_uuid}}/cameras/all/tasks?page.offset=0&page.limit=100

# page.limit  一页显示多少个任务
# page.offset  偏移量
# 举例：查询0-100任务 page.offset=0&page.limit=100
        查询100-200任务 page.offset=100&page.limit=100
        查询60-100任务 page.offset=60&page.limit=40
```
关注字段：<br/>
status：任务状态<br/>
error_message：错误信息<br/>

### 9、查看消费组设置
* 一套viper对应多个业务（一套sensecity一套skyline也算）
```
需要修改业务kafka的consumer-group，默认的consumer-group是相同的，导致部分数据消费不到<br/>

修改方式：修改配置文件whale-collective.yml中groupId、groupId2、groupId3、crowd-group的值，使得几个业务层的该值不一致即可（比如在原来的值后面加上当前服务器的ip）<br/>
```

然后重启whale-collective:
```bash 
kubectl delete pod -n whale-component whale-collective-xxxxxxxxx-xxxxx
```



## 创建照片布控任务失败
### 1、查看whale-portrait日志，若提示新建私有人像库失败
```
* 修改senseface.info_target_library中name='私有人像库'的数据中status值为2
* 清除redis(cluster那套redis)里面私有人像库缓存 Whale:TargetLibrary:Private
* 服务不用重启
```
### 2、查看whale-portrait日志，若提示找不到bucket
```
一般为底层osg没有挂载nas导致；处理问题前先确认osg已挂载nas
* 方法一：调用底层接口(PUT /components/osg-default/v1)，手动创建新的bucket
* 方法二：修改senseface.info_target_library中name='私有人像库'的数据中status值为2；清除redis(cluster那套redis)里面私有人像库缓存 Whale:TargetLibrary:Private
```


## 导出布控告警结果失败
```
一般为导出的数据过大导致，可以尝试一次性导出少一点告警数据
```


## v5.1.1及以上版本，图片为第三方，告警后存到es里面的url只有一半

解决办法：

1. 修改whale-collective.yaml配置enableUrlPrefix: false为true
2. 修改whale-realtime.yaml配置persistentAlert: false为true
3. 修改whale-realtime.yaml配置whale.common.fileSystem.type: minio为direct
4. 重启whale-collective服务
5. 重启realtime服务（sc_ip:18081页面将job给cancel掉，等待5min，任务会自动拉起）


## 布控告警时间与抓拍时间不一致

1. 查看底层服务器时间是否正确;
2. 用告警id去es中查询是否有数据;
3. 查看es中的告警时间（receivedTime）是否正确;
4. 消费告警数据，查看返回的告警是否正确。
5. 如果确认是bug提fit单给对应产品接口人。

## v5.1.1 realtime调优
### 1、登录`nacos`，修改`whale-realtime.yaml`文件
```

flink:
    defaultParallelism: 8     # 原来是16
    startUps: FACE_ALARM,CAPTURE,CROWD,MAP_CENTER,ALGO,EVENT_ALARM,BODY_ALARM,VEHICLE_ALARM,WIRELESS_TERMINAL_ALARM,COMB_FACE_ALARM,GARBAGE,SITUATION_ALARM # 这个是新增的配置
kafka:
    capture:                  # 新增的配置
        groupId: whaleRealTimeCaptureGroup
        bodyTopic: WhaleBodyCollect
        faceTopic: WhaleCollect
        pushTopic: GeneralPush
face:
    group:
        identitySearchPrepareGroup:
            enable: true    # 原来是false
            parallelism: 16 # 新增配置
        identitySearchGroup:
            enable: true    # 原来是false
            parallelism: 16 # 新增配置
        alarmUsingGroup:
            enable: true    # 原来是false
            parallelism: 16 # 新增配置

```
### 2、配置修改好之后发布，然后登录`flink`页面，将`job`给`cancel`掉
**一般情况下，等待5min，realtime-job会自动拉起，若没有自动拉起，可以执行下面的命令手动拉起**
```
kubectl apply -f /etc/kubernetes/apps/business-streamingjob/centurio-streamingjob/job.yml
```

## 没有布控审批权限的普通用户新建布控任务后，新建的任务在哪看

没有布控审批权限的用户，新建完布控任务后，可到个人中心-全部事项查看任务的状态；进入任务详情，显示审批的进度、审批人。


