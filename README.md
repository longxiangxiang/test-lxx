# test-lxx

lxx 测试中心

## 本地调试方法

官方文档: https://www.mkdocs.org/getting-started/

1. 安装软件
  
    **前提：安装Python**

    通过命令`pip install mkdocs`安装 mkdocs

2. 运行软件

    在项目根目录下运行一下命令即可在本地调试

    ```bash
    mkdocs serve
    ```
